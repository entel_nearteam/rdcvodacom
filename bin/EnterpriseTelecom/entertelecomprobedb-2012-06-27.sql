-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Client: 127.0.0.1
-- Généré le : Mer 27 Juin 2012 à 18:24
-- Version du serveur: 5.5.16
-- Version de PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `entertelecomprobedb`
--

-- --------------------------------------------------------

--
-- Structure de la table `destination_map`
--

CREATE TABLE IF NOT EXISTS `destination_map` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MAP_GROUP` varchar(24) DEFAULT NULL,
  `PREFIX` varchar(24) DEFAULT NULL,
  `TIER_CODE` varchar(24) DEFAULT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `CATEGORY` varchar(50) DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Contenu de la table `destination_map`
--

INSERT INTO `destination_map` (`ID`, `MAP_GROUP`, `PREFIX`, `TIER_CODE`, `DESCRIPTION`, `CATEGORY`) VALUES
(1, 'EMTEL_CS', '230', 'MAURITIUS_FIXED', 'MAURITIUS_FIXED', 'Fixed'),
(2, 'EMTEL_CS', '230', 'MAURITIUS_FIXED', 'MAURITIUS_FIXED', 'Fixed'),
(3, 'EMTEL_CS', '230421', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(4, 'EMTEL_CS', '230422', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(5, 'EMTEL_CS', '230423', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(6, 'EMTEL_CS', '230428', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(7, 'EMTEL_CS', '230429', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(8, 'EMTEL_CS', '23049', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(9, 'EMTEL_CS', '23071', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(10, 'EMTEL_CS', '23072', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(11, 'EMTEL_CS', '23073', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(12, 'EMTEL_CS', '23074', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(13, 'EMTEL_CS', '23097', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(14, 'EMTEL_CS', '23098', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(15, 'EMTEL_CS', '23025', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(16, 'EMTEL_CS', '2307', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(17, 'EMTEL_CS', '230875', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(18, 'EMTEL_CS', '230876', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(19, 'EMTEL_CS', '230877', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(20, 'EMTEL_CS', '23091', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(21, 'EMTEL_CS', '23094', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(22, 'EMTEL_CS', '23095', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(23, 'EMTEL_IGW', '230', 'MAURITIUS_FIXED', 'MAURITIUS_FIXED', 'Fixed'),
(24, 'EMTEL_IGW', '230', 'MAURITIUS_FIXED', 'MAURITIUS_FIXED', 'Fixed'),
(25, 'EMTEL_IGW', '230421', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(26, 'EMTEL_IGW', '230422', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(27, 'EMTEL_IGW', '230423', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(28, 'EMTEL_IGW', '230428', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(29, 'EMTEL_IGW', '230429', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(30, 'EMTEL_IGW', '23049', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(31, 'EMTEL_IGW', '23071', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(32, 'EMTEL_IGW', '23072', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(33, 'EMTEL_IGW', '23073', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(34, 'EMTEL_IGW', '23074', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(35, 'EMTEL_IGW', '23097', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(36, 'EMTEL_IGW', '23098', 'MAURITIUS_MOBILE_EMTEL', 'MAURITIUS_MOBILE_EMTEL', 'Mobile'),
(37, 'EMTEL_IGW', '23025', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(38, 'EMTEL_IGW', '2307', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(39, 'EMTEL_IGW', '230875', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(40, 'EMTEL_IGW', '230876', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(41, 'EMTEL_IGW', '230877', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(42, 'EMTEL_IGW', '23091', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(43, 'EMTEL_IGW', '23094', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(44, 'EMTEL_IGW', '23095', 'MAURITIUS_MOBILE_OTHERS', 'MAURITIUS_MOBILE_OTHERS', 'Mobile'),
(45, 'Quintum', '224', 'GUI', 'GUI', 'Mobile'),
(46, 'Quintum', '22424', 'GUI_MTN1', 'GUI_MTN1', 'Mobile'),
(47, 'Quintum', '22464', 'GUI_MTN2', 'GUI_MTN2', 'Mobile'),
(48, 'Quintum', '22466', 'GUI_MTN3', 'GUI_MTN3', 'Mobile');

-- --------------------------------------------------------

--
-- Structure de la table `norm_map`
--

CREATE TABLE IF NOT EXISTS `norm_map` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MAP_GROUP` varchar(24) DEFAULT NULL,
  `BAND` varchar(24) DEFAULT NULL,
  `NUMBER` varchar(24) DEFAULT NULL,
  `OLD_PREFIX` varchar(24) DEFAULT NULL,
  `NEW_PREFIX` varchar(24) DEFAULT NULL,
  `RANK` int(11) DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Contenu de la table `norm_map`
--

INSERT INTO `norm_map` (`ID`, `MAP_GROUP`, `BAND`, `NUMBER`, `OLD_PREFIX`, `NEW_PREFIX`, `RANK`) VALUES
(15, 'EMTEL_CS', '.*', '\\+230.*', '^\\+230', '230', 1),
(16, 'EMTEL_IGW', '.*', '\\+230.*', '^\\+230', '230', 1),
(17, 'EMTEL_CS', '.*', '230.*', '', '', 2),
(18, 'EMTEL_IGW', '.*', '230.*', '', '', 2),
(19, 'EMTEL_IGW', '.*', '[0-9]*', '', '230', 3),
(20, 'EMTEL_CS', '.*', '[0-9]*', '', '230', 3),
(21, 'EMTEL_IGW', '.*', '.*', '', '', 4),
(22, 'EMTEL_CS', '.*', '.*', '', '', 4),
(23, 'Quintum', '.*', '.*', '', '', 5);

-- --------------------------------------------------------

--
-- Structure de la table `probe_filter`
--

CREATE TABLE IF NOT EXISTS `probe_filter` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MAP_GROUP` varchar(24) NOT NULL,
  `CALLING_NUMBER` varchar(64) NOT NULL,
  `CALLED_NUMBER` varchar(64) NOT NULL,
  `IN_TRUNK` varchar(64) NOT NULL,
  `OUT_TRUNK` varchar(64) NOT NULL,
  `DESTINATION` varchar(64) NOT NULL,
  `START_DATE` date NOT NULL DEFAULT '2000-01-01',
  `END_DATE` date NOT NULL DEFAULT '2030-01-01',
  `IS_PROBE` char(1) DEFAULT NULL,
  `RANK` int(11) DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `probe_filter`
--

INSERT INTO `probe_filter` (`ID`, `MAP_GROUP`, `CALLING_NUMBER`, `CALLED_NUMBER`, `IN_TRUNK`, `OUT_TRUNK`, `DESTINATION`, `START_DATE`, `END_DATE`, `IS_PROBE`, `RANK`) VALUES
(1, 'EMTEL_CS', '.*', '2309743901', '.*', '.*', '.*', '2000-01-01', '2030-01-01', 'y', 1),
(2, 'EMTEL_IGW', '.*', '2309743901', '.*', '.*', '.*', '2000-01-01', '2030-01-01', 'Y', 1),
(3, 'Quintum', '.*', 'f18b44620c762386232a6074b5cde5d5', '.*', '.*', 'GUI_MTN2', '2000-01-01', '2030-01-01', 'y', 1),
(4, 'Quintum', '.*', 'abe549764af776743725220223ad6455', '.*', '.*', 'GUI_MTN2', '2000-01-01', '2030-01-01', 'y', 1);

-- --------------------------------------------------------

--
-- Structure de la table `suspense_map`
--

CREATE TABLE IF NOT EXISTS `suspense_map` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ERROR_CODE` varchar(64) NOT NULL,
  `MAP_GROUP` varchar(24) NOT NULL,
  `OUTPUT_NAME` varchar(24) NOT NULL,
  `RANK` int(11) NOT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Contenu de la table `suspense_map`
--

INSERT INTO `suspense_map` (`ID`, `ERROR_CODE`, `MAP_GROUP`, `OUTPUT_NAME`, `RANK`) VALUES
(1, '.*', 'Probe', 'SuspenseOutput', 99),
(2, 'ERR_UNBILLED_RECORD', 'Probe', 'DiscardOutput', 1);

-- --------------------------------------------------------

--
-- Structure de la table `trunk_map`
--

CREATE TABLE IF NOT EXISTS `trunk_map` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MAP_GROUP` varchar(24) NOT NULL,
  `TRUNK` varchar(64) NOT NULL,
  `TRUNK_NAME` varchar(24) NOT NULL,
  `TRUNK_ID` varchar(64) NOT NULL,
  `START_DATE` date NOT NULL DEFAULT '2000-01-01',
  `END_DATE` date NOT NULL DEFAULT '2030-01-01',
  `RANK` int(11) DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Contenu de la table `trunk_map`
--

INSERT INTO `trunk_map` (`ID`, `MAP_GROUP`, `TRUNK`, `TRUNK_NAME`, `TRUNK_ID`, `START_DATE`, `END_DATE`, `RANK`) VALUES
(1, 'EMTEL_IGW', '0', 'BELGIUM:BELGACOM 1 ', 'IDD voice calls via ILD Belgacom1 operator', '2000-01-01', '2030-01-01', 1),
(2, 'EMTEL_IGW', '1', 'BELGIUM:BELGACOM 2 ', 'IDD voice calls via ILD Belgacom2 operator', '2000-01-01', '2030-01-01', 1),
(3, 'EMTEL_IGW', '2', 'USA:TELCO 214', 'IDD voice calls via ILD Telco operator', '2000-01-01', '2030-01-01', 1),
(4, 'EMTEL_IGW', '3', 'GERMANY:DEUTCHE TELECOM', 'IDD voice calls via ILD Deutche Telecom operator', '2000-01-01', '2030-01-01', 1),
(5, 'EMTEL_IGW', '4', 'REUNION:SRR', 'IDD voice calls via ILD SRR operator', '2000-01-01', '2030-01-01', 1),
(6, 'EMTEL_IGW', '6', 'CELLPLUS IDD: PLS', 'Orange voice calls', '2000-01-01', '2030-01-01', 1),
(7, 'EMTEL_IGW', '7', 'MAURITIUS TELECOM PLS', 'MT Port Louis voice calls', '2000-01-01', '2030-01-01', 1),
(8, 'EMTEL_IGW', '8', 'MAURITIUS TELECOM RSH', 'MT Rose Hill voice calls', '2000-01-01', '2030-01-01', 1),
(9, 'EMTEL_IGW', '9', 'MAURITIUS TELECOM FLO', 'MT Floreal voice calls', '2000-01-01', '2030-01-01', 1),
(10, 'EMTEL_IGW', '14', 'INDIA:BHARATI', 'IDD voice calls via ILD Bharati operator', '2000-01-01', '2030-01-01', 1),
(11, 'EMTEL_IGW', '10', 'MAHANAGAR', 'MTML voice calls', '2000-01-01', '2030-01-01', 1),
(12, 'EMTEL_IGW', '11', 'IGW-MSOFT ', 'IGW-MSOFT - voice calls', '2000-01-01', '2030-01-01', 1),
(13, 'EMTEL_IGW', '12', 'IGW-VOIP  ', 'IGW-VOIP - voice calls', '2000-01-01', '2030-01-01', 1),
(14, 'EMTEL_IGW', '20', 'BELGIUM:BELGACOM 1 ', 'IDD Video calls via ILD Belgacom1 operator', '2000-01-01', '2030-01-01', 1),
(15, 'EMTEL_IGW', '21', 'BELGIUM:BELGACOM 2 ', 'IDD Video calls via ILD Belgacom2 operator', '2000-01-01', '2030-01-01', 1),
(16, 'EMTEL_IGW', '110', 'IGW-MSOFT ', 'IGW - MSOFT - video calls', '2000-01-01', '2030-01-01', 1),
(17, 'EMTEL_IGW', '120', 'IGW-VOIP ', 'IGW - VOIP - video calls', '2000-01-01', '2030-01-01', 1),
(18, 'EMTEL_CS', 'HEIMTFLOT0', 'HEIMTFLOT0', 'IDD calls from local ILD oper via Floreal exch', '2000-01-01', '2030-01-01', 1),
(19, 'EMTEL_CS', 'HEIMTPORT0', 'HEIMTPORT0', 'IDD calls from local ILD oper via Port Louis exch', '2000-01-01', '2030-01-01', 1),
(20, 'EMTEL_CS', 'HEIMTROST0', 'HEIMTROST0', 'IDD calls from local ILD oper via Rose Hill exch', '2000-01-01', '2030-01-01', 1),
(21, 'EMTEL_CS', 'HEIMTSAFT0', 'HEIMTSAFT0', 'Inc IDD CALLS ( Safe cable at Baie Jocotet )', '2000-01-01', '2030-01-01', 1),
(22, 'EMTEL_CS', 'HEIMTCAST0', 'HEIMTCAST0', 'Inc IDD CALLS ( Satelite link at Cassis )', '2000-01-01', '2030-01-01', 1),
(23, 'EMTEL_CS', 'HEIMAHANT1', 'HEIMAHANT1', 'M ( Emtel ) <----> ILD ( 060/061 ) MTML', '2000-01-01', '2030-01-01', 1),
(24, 'EMTEL_CS', 'HEICTCALT0', 'HEICTCALT0', 'IDD calls to & from local ILD operator', '2000-01-01', '2030-01-01', 1),
(25, 'EMTEL_CS', 'HEIDCLLTT0', 'HEIDCLLTT0', 'IDD calls to & from local ILD operator', '2000-01-01', '2030-01-01', 1),
(26, 'EMTEL_CS', 'HEIDCLLTT1', 'HEIDCLLTT1', 'IDD calls to & from local ILD operator', '2000-01-01', '2030-01-01', 1),
(27, 'EMTEL_CS', 'HEIHTLNKT0', 'HEIHTLNKT0', 'IDD calls to & from local ILD operator', '2000-01-01', '2030-01-01', 1),
(28, 'EMTEL_CS', 'HEITLCLTT0', 'HEITLCLTT0', 'IDD calls to & from local ILD operator', '2000-01-01', '2030-01-01', 1),
(29, 'EMTEL_CS', 'HEITMDIAT0', 'HEITMDIAT0', 'IDD calls to & from local ILD operator', '2000-01-01', '2030-01-01', 1),
(30, 'EMTEL_CS', 'HIPIGTWYT0', 'HIPIGTWYT0', 'Voice traffic between MSC and Emtel IGW', '2000-01-01', '2030-01-01', 1),
(31, 'EMTEL_CS', 'HIPIGTWYV0', 'HIPIGTWYV0', 'Video traffic between MSC and Emtel IGW', '2000-01-01', '2030-01-01', 1),
(32, 'EMTEL_IGW', '.*', 'UNKNOWN', 'Unknown', '2000-01-01', '2030-01-01', 99),
(33, 'EMTEL_CS', '.*', 'UNKNOWN', 'Unknown', '2000-01-01', '2030-01-01', 99),
(34, 'Quintum', '87\\.98\\.199\\.8', 'Acti', 'Customer_A', '2000-01-01', '2030-01-01', 5),
(35, 'Quintum', '217\\.117\\.152\\.123', 'Elast', 'Provider_A', '2000-01-01', '2030-01-01', 5),
(36, 'Quintum', '193\\.41\\.238\\.[0-9]*', 'LongP', 'Customer_B', '2000-01-01', '2030-01-01', 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
