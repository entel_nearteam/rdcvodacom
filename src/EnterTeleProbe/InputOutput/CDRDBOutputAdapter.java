/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Ltd 2006-2010.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of architecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are disclaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
/* ========================== VERSION HISTORY =========================
 * $Log: ProbeCDRDBOutputAdapter.java,v $
 * Revision 1.3  2013-08-05 21:12:48  jean-baptiste
 * Last Report Version - slow pb
 *
 * Revision 1.2  2012-07-07 15:59:04  ian
 * Relate tables, add timestamp rounding, refactor
 *
 * Revision 1.1  2012-07-07 13:55:58  ian
 * Refactor for clarity
 *
 * Revision 1.3  2012-06-27 16:23:07  jean-baptiste
 * - New Call layer cdr format + new SQL structure
 * - PDD added in probe cdr
 *
 * Revision 1.2  2012-06-24 22:12:00  ian
 * Rating WIP
 *
 * ====================================================================
 */
package EnterTeleProbe.InputOutput;

import EnterTeleProbe.ProbeRecord;
import OpenRate.adapter.jdbc.JDBCOutputAdapter;
import OpenRate.record.DBRecord;
import OpenRate.record.ErrorType;
import OpenRate.record.IRecord;
import OpenRate.record.RecordError;
import java.util.ArrayList;
import java.util.Collection;


/**
 *
 * @author IanAdmin
 */
public class CDRDBOutputAdapter extends JDBCOutputAdapter
{
  // this is the CVS version info
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: ProbeCDRDBOutputAdapter.java,v $, $Revision: 1.3 $, $Date: 2013-08-05 21:12:48 $";

  /** Creates a new instance of DBOutput */
  public CDRDBOutputAdapter()
  {
    super();
  }

  /**
   * We transform the records here so that they are ready to output making any
   * specific changes to the record that are necessary to make it ready for
   * output.
   *
   * As we are using the JDBCOutput adapter, we should transform the records
   * into DBRecords, storing the data to be written using the SetData() method.
   * This means that we do not have to know about the internal workings of the
   * output adapter.
   *
   * Note that this is just undoing the transformation that we did in the input
   * adapter.
   */
    @Override
    public Collection<IRecord> procValidRecord(IRecord r)
    {
      DBRecord tmpDataRecord;
      ProbeRecord tmpInRecord;

      Collection<IRecord> Outbatch;
      Outbatch = new ArrayList<IRecord>();
      tmpInRecord = (ProbeRecord)r;
      tmpDataRecord = new DBRecord();
      
      tmpDataRecord.setOutputColumnCount(22);
      tmpDataRecord.setOutputColumnInt   ( 0,tmpInRecord.fileNameID);
      tmpDataRecord.setOutputColumnInt   ( 1,tmpInRecord.RecordNumber);
      tmpDataRecord.setOutputColumnString( 2,tmpInRecord.mapGroup);
      tmpDataRecord.setOutputColumnString( 3,tmpInRecord.recordDate);
      tmpDataRecord.setOutputColumnString( 4,tmpInRecord.recordTime);
      tmpDataRecord.setOutputColumnLong  ( 5,tmpInRecord.PDD);      
      tmpDataRecord.setOutputColumnLong  ( 6,tmpInRecord.duration);      
      tmpDataRecord.setOutputColumnString( 7,tmpInRecord.callingNumber);
      tmpDataRecord.setOutputColumnString( 8,tmpInRecord.calledNumber);
      tmpDataRecord.setOutputColumnString( 9,tmpInRecord.normCalledNumber);
      tmpDataRecord.setOutputColumnString(10,tmpInRecord.BTS);
      tmpDataRecord.setOutputColumnString(11,tmpInRecord.LAC);
      tmpDataRecord.setOutputColumnString(12,tmpInRecord.IMEI);
      tmpDataRecord.setOutputColumnString(13,tmpInRecord.IMSI);
      tmpDataRecord.setOutputColumnString(14,tmpInRecord.inOperator);
      tmpDataRecord.setOutputColumnString(15,tmpInRecord.inTrunkId);
      tmpDataRecord.setOutputColumnString(16,tmpInRecord.outOperator);
      tmpDataRecord.setOutputColumnString(17,tmpInRecord.outTrunkId);
      tmpDataRecord.setOutputColumnString(18,tmpInRecord.TrunkTypeID);
      tmpDataRecord.setOutputColumnString(19,tmpInRecord.CallTypeID);
      tmpDataRecord.setOutputColumnString(20,tmpInRecord.OriginID);
      tmpDataRecord.setOutputColumnString(21,tmpInRecord.DestinationID);
      
      Outbatch.add((IRecord)tmpDataRecord);

      return Outbatch;
    }

  /**
   * Handle any error records here so that they are ready to output making any
   * specific changes to the record that are necessary to make it ready for
   * output.
   */
    @Override
    public Collection<IRecord> procErrorRecord(IRecord r)
    {
      return null;
    }
}
