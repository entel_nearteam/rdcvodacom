/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Ltd 2006-2012.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of arhitecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are discplaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
/* ========================== VERSION HISTORY =========================
 * $Log: SetOutput.java,v $
 * Revision 1.4  2012-01-26 09:13:27  ian
 * Update for filtering
 *
 * Revision 1.3  2012/01/17 12:54:51  ian
 * Add trunk mapping
 *
 * Revision 1.2  2012/01/17 11:55:13  ian
 * Map CS and IGW types
 *
 * Revision 1.1  2012/01/16 22:51:53  ian
 * Initial Starting Version
 *
 * ====================================================================
 */
package EnterTeleProbe.InputOutput;

import EnterTeleProbe.ProbeRecord;
import OpenRate.exception.ProcessingException;
import OpenRate.process.AbstractStubPlugIn;
import OpenRate.record.IRecord;
import OpenRate.record.RecordError;
import java.util.Iterator;

/**
 *
 * @author ian
 */
public class SetOutput extends AbstractStubPlugIn
{
  // this is the CVS version info
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: SetOutput.java,v $, $Revision: 1.4 $, $Date: 2012-01-26 09:13:27 $";
    
  @Override
  public IRecord procValidRecord(IRecord r) throws ProcessingException
  {
    ProbeRecord CurrentRecord = (ProbeRecord)r;

    if ((CurrentRecord.RECORD_TYPE == ProbeRecord.PROBE_RECORD) || (CurrentRecord.RECORD_TYPE == ProbeRecord.LIVE_TRACK_RECORD))
    {
      CurrentRecord.addOutput("GoodOutput");
      CurrentRecord.GoodOutput = true;
      CurrentRecord.DiscardOutput = false;
    }
    else if (CurrentRecord.RECORD_TYPE == ProbeRecord.HEADER_RECORD)
    {
      CurrentRecord.GoodOutput = true;
      CurrentRecord.DiscardOutput = true;
    }
    else if (CurrentRecord.RECORD_TYPE == ProbeRecord.TRAILER_RECORD)
    {
      CurrentRecord.GoodOutput = true;
      CurrentRecord.DiscardOutput = true;
    }    
    
    return r;
  }

  @Override
  public IRecord procErrorRecord(IRecord r) throws ProcessingException
  {
    ProbeRecord CurrentRecord = (ProbeRecord)r;
    RecordError tmpError;
    boolean toReject = false;
    String errorList;

    if (CurrentRecord.RECORD_TYPE == ProbeRecord.PROBE_RECORD)
    {
      errorList = "";
      
      // See if we have a discard error in there
      Iterator errorIter = CurrentRecord.getErrors().iterator();
      
      while (errorIter.hasNext())
      {
        tmpError = (RecordError) errorIter.next();
        errorList = errorList + tmpError.getMessage() + ":";
        if (tmpError.getMessage().startsWith("ERR"))
        {
          toReject = true;
        }
      }
      
      // send to the reject output if there was at least 1 error message
      if (toReject)
      {
        CurrentRecord.addOutput("RejectOutput");
        CurrentRecord.GoodOutput = false;
        CurrentRecord.DiscardOutput = true;
      }
      else
      {
        CurrentRecord.addOutput("GoodOutput");
        CurrentRecord.GoodOutput = true;
        CurrentRecord.DiscardOutput = false;
      }      
    }
    return r;
  }
  
}