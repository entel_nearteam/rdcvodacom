/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Ltd 2006-2012.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of arhitecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are discplaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
/* ========================== VERSION HISTORY =========================
 * $Log: AirtelHWProbeInput.java,v $
 * Revision 1.1  2014/10/07 08:13:05  florent
 * ICH : ajout du decodage Huawei, differenciation des aggreations, ajout de l'output csv, corrections diverses
 *
 * Revision 1.1.1.1  2014/09/05 09:35:24  florent
 * Airtel ASN MO+MT
 *
 * Revision 1.2  2014/07/31 16:11:55  florent
 * Auto learning
 *
 * Revision 1.3  2012-07-25 16:59:03  jean-baptiste
 * new time rounding + agg
 *
 * Revision 1.2  2012-07-12 05:25:46  ian
 * Code review
 *
 * Revision 1.1  2012-05-22 12:37:00  jean-baptiste
 * Loader Record for Cyrille
 *
 * Revision 1.2  2012-03-24 14:32:55  ian
 * Add obfuscation, minor improvements
 *
 * Revision 1.1  2012/01/17 11:55:13  ian
 * Map CS and IGW types
 *
 * Revision 1.1  2012/01/16 22:51:53  ian
 * Initial Starting Version
 *
 * ====================================================================
 */
package EnterTeleProbe.Customers;

import EnterTeleProbe.ProbeRecord;
import OpenRate.adapter.file.FlatFileInputAdapter;
import OpenRate.record.ErrorType;
import OpenRate.record.FlatRecord;
import OpenRate.record.HeaderRecord;
import OpenRate.record.IRecord;
import OpenRate.record.RecordError;
import OpenRate.record.TrailerRecord;

/**
 * This module processed the data from the file and determines the mapping
 * which should be applied to it.
 *
 * @author Afzaal
 */
public class VodaRdcSSPProbeInput
  extends FlatFileInputAdapter
{
  /**
   * CVS version info - Automatically captured and written to the Framework
   * Version Audit log at Framework startup. For more information
   * please <a target='new' href='http://www.open-rate.com/wiki/index.php?title=Framework_Version_Map'>click here</a> to go to wiki page.
   */
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: AirtelHWProbeInput.java,v $, $Revision: 1.1 $, $Date: 2014/10/07 08:13:05 $";

  //  This is the stream record number counter which tells us the number of the compressed records
  private int StreamRecordNumber;
  
  // This is the object that is used to compress the records
  ProbeRecord tmpDataRecord = null;

  // Used for aggregation
  String streamName;
  boolean IsFirstCDR ;
  
  
 /**
  * Constructor for ErgatelInput.
  */
  public VodaRdcSSPProbeInput()
  {
    super();
  }
  
  //-----------------------------------------------------------------------------
  // ------------------ Start of inherited Plug In functions ---------------------
  // -----------------------------------------------------------------------------

 /**
  * This is called when the synthetic Header record is encountered, and has the
  * meanining that the stream is starting. In this example we have nothing to do
  */
  @Override
  public IRecord procHeader(IRecord r)
  {
    HeaderRecord hdr = (HeaderRecord) r;
    
    // reset the record numbering
    StreamRecordNumber = 1;
    IsFirstCDR         = true;
    
    // get the stream name so we know the file where each record came
    streamName = this.inputFilePrefix + hdr.getStreamName() + this.inputFileSuffix;

    return r;
  }

 /**
  * This is called when a data record is encountered. This input adapter
  * performs record compression, putting the sub records under the
  * D record. Other records pass straight through.
  */
  @Override
  public IRecord procValidRecord(IRecord r)
  {
    String tmpData;
    FlatRecord originalRecord = (FlatRecord)r;
    tmpData = originalRecord.getData();
    // Used for wrong CDR checking
    try
    {
      String tmpvar[] = tmpData.split(";");
      String tmpNum = tmpvar[1];
      if ((tmpNum.length()== 0) || (!tmpData.matches(".*[[a-zA-Z_0-9],].*")))
      {
         // Drop it
          return null;
      }
    }
    catch (NullPointerException e)
    {
       return null;
    }
    catch (ArrayIndexOutOfBoundsException e)        
     {
         // Drop it
          return null;
      }        
    
 
    if (tmpData.startsWith(ProbeRecord.HEADER_TRAILER_ID))
    {
      // Create a new record
      tmpDataRecord = new ProbeRecord();
      
      tmpDataRecord.mapHeaderTrailerRecord(tmpData);
      tmpDataRecord.RecordNumber = StreamRecordNumber++;
    }
    else
    {
      // Create a new record
      try{
      tmpDataRecord = new ProbeRecord();
      tmpDataRecord.fileName          = streamName;
      
      tmpDataRecord.mapSSPProbeRecord(tmpData);
      if ((!tmpDataRecord.CallType.equals("MO")) && (!tmpDataRecord.CallType.equals("MT")))
      { //on ne traite que des MO
          return null;
      }
      tmpDataRecord.cdrID             = streamName +"-"+ tmpDataRecord.RecordNumber; //CDR ID fourni par le switch
      tmpDataRecord.RecordNumber      = StreamRecordNumber++;
      if (IsFirstCDR == true)
      {
          tmpDataRecord.isNewFilename = true; //On stocke le filename 1 fois
          IsFirstCDR                  = false;
      }
      
      }
      catch (Exception e)
              {
                   RecordError tmpError = new RecordError("ERR_UNDEFINED ligne:" + StreamRecordNumber, ErrorType.DATA_VALIDATION);
                    tmpDataRecord.addError(tmpError);
              }
        
    }

    return (IRecord)tmpDataRecord;
  }
  
 /**
  * This is called when a data record with errors is encountered.
  * 
  * The input adapter is not expected to provide any records here.
  */
  @Override
  public IRecord procErrorRecord(IRecord r)
  {
    // The FlatFileInputAdapter is not able to create error records, so we
    // do not have to do anything for this
    return r;
  }

 /**
  * This is called when the synthetic trailer record is encountered, and has the
  * meanining that the stream is now finished. In this example, all we do is 
  * pass the control back to the transactional layer.
  *
  * In models where record aggregation (records and sub records) is used, you
  * might want to check for any purged records here.
  */
  @Override
  public IRecord procTrailer(IRecord r)
  {
    TrailerRecord tmpTrailer;
    
    // set the trailer record count
    tmpTrailer = (TrailerRecord)r;
    
    // reset the stream name
    streamName = null;
    
    tmpTrailer.setRecordCount(StreamRecordNumber);
    return (IRecord)tmpTrailer;
  }
}
