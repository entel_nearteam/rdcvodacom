package EnterTeleProbe;

import java.util.ArrayList;

public class TrunksSingleton {

	private static TrunksSingleton trunks=null;
	
	private ArrayList<String> trunkTypes = new ArrayList<String>();
	
	private TrunksSingleton()
	{
		//international trunks
		trunkTypes.add("14003");
		trunkTypes.add("14005");
		trunkTypes.add("FRT02");
		trunkTypes.add("FRT01");
		trunkTypes.add("14006");
		trunkTypes.add("14008");
		trunkTypes.add("BLG4F");
		trunkTypes.add("AFKIL");
		trunkTypes.add("BLG4B");
		trunkTypes.add("MTNBZ");
		trunkTypes.add("AFKL2");
		trunkTypes.add("BLG3B");
		trunkTypes.add("ISA2B");
		trunkTypes.add("BLG3F");
		trunkTypes.add("ISA1B");
		trunkTypes.add("22002");
		trunkTypes.add("14001");
		trunkTypes.add("blg3i");
		trunkTypes.add("blg6f");
		trunkTypes.add("14008");
		trunkTypes.add("IDT02");
		trunkTypes.add("blg5f");
		trunkTypes.add("blg4i");
		trunkTypes.add("53009");
		trunkTypes.add("22004");
		trunkTypes.add("54013");
		trunkTypes.add("54015");
		trunkTypes.add("53010");
		trunkTypes.add("54009");
		trunkTypes.add("52008");
		trunkTypes.add("22005");
		trunkTypes.add("22007");
		trunkTypes.add("22003");
		trunkTypes.add("22003");
		trunkTypes.add("54012");
		trunkTypes.add("53008");
		trunkTypes.add("54014");
		trunkTypes.add("54010");
		trunkTypes.add("54008");
		trunkTypes.add("54011");
		trunkTypes.add("52009");
		trunkTypes.add("14009");

	}
	public static TrunksSingleton getInstance()
	{
		if (trunks == null) {
			
			trunks = new TrunksSingleton();
		}
		return trunks;
	}
	
	public ArrayList<String> getTrunkTypes()
	{
		return trunkTypes;
	}
	
	
}
