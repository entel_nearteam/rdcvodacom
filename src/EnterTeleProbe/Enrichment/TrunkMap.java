/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Limited 2006-2012.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of architecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are discplaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
/* ========================== VERSION HISTORY =========================
 * $Log: TrunkMap.java,v $
 * Revision 1.3  2012-07-12 15:43:55  ian
 * Robustness
 *
 * Revision 1.2  2012/01/18 12:35:56  ian
 * Add normlisation and zoning
 *
 * Revision 1.1  2012/01/17 12:54:51  ian
 * Add trunk mapping
 *
 * ====================================================================
 */
package EnterTeleProbe.Enrichment;

import EnterTeleProbe.ProbeRecord;
import OpenRate.process.AbstractRegexMatch;
import OpenRate.record.ErrorType;
import OpenRate.record.IRecord;
import OpenRate.record.RecordError;
import java.util.ArrayList;

/**
 * This class will get the Rate Type either base or optional using Regex. This
 * is only needed for originating traffic with customer specific tariffs. Others
 * already have suffucuent information
 */
public class TrunkMap
  extends AbstractRegexMatch
{
  // this is the CVS version info
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: TrunkMap.java,v $, $Revision: 1.3 $, $Date: 2012-07-12 15:43:55 $";

  // String array used for searching
  private String[] tmpSearchParameters = new String[1];

  // -----------------------------------------------------------------------------
  // ------------------ Start of inherited Plug In functions ---------------------
  // -----------------------------------------------------------------------------

 /**
  * Lookup trunk ID and Name from the input trunk info
  */
  @Override
  public IRecord procValidRecord(IRecord r)
  {
    ArrayList<String> RegexResult;
    RecordError tmpError;
    ProbeRecord CurrentRecord = (ProbeRecord) r;

    if (CurrentRecord.RECORD_TYPE == ProbeRecord.PROBE_RECORD)
    {
      if ((CurrentRecord.inOperator == null) || CurrentRecord.inOperator.isEmpty())
      {
        CurrentRecord.inTrunkId = "";
        CurrentRecord.inTrunkName = "";
        CurrentRecord.trunkType = "";
      }
      else
      {
        // Look up the rate model to use
        tmpSearchParameters[0] = CurrentRecord.inOperator;
        RegexResult = getRegexMatchWithChildData(CurrentRecord.mapGroup,tmpSearchParameters);
        // Double check with Default value
        if (!isValidRegexMatchResult(RegexResult)) {RegexResult = getRegexMatchWithChildData("Default",tmpSearchParameters);}
        
        if (isValidRegexMatchResult(RegexResult))
        {
          // Enrich the record
          CurrentRecord.inTrunkName = RegexResult.get(0);
          CurrentRecord.inTrunkId = RegexResult.get(1);
          CurrentRecord.trunkType = RegexResult.get(2);
        }
        else
        {
          tmpError = new RecordError("ERR_IN_TRUNK_MAP", ErrorType.SPECIAL,CurrentRecord.inOperator);
          CurrentRecord.addError(tmpError);
        }
      }
    
      if ((CurrentRecord.outOperator == null) || CurrentRecord.outOperator.isEmpty())
      {
        CurrentRecord.outTrunkId = "";
        CurrentRecord.outTrunkName = "";
      }
      else
      {
        // Look up the rate model to use
        tmpSearchParameters[0] = CurrentRecord.outOperator;
        RegexResult = getRegexMatchWithChildData(CurrentRecord.mapGroup,tmpSearchParameters);
        if (!isValidRegexMatchResult(RegexResult)) RegexResult = getRegexMatchWithChildData("Default",tmpSearchParameters);
        if (isValidRegexMatchResult(RegexResult))
        {
          // Enrich the record
          CurrentRecord.outTrunkName = RegexResult.get(0);
          CurrentRecord.outTrunkId = RegexResult.get(1);
        }
        else
        {
          tmpError = new RecordError("ERR_OUT_TRUNK_MAP", ErrorType.SPECIAL,CurrentRecord.outOperator);
          CurrentRecord.addError(tmpError);
        }
      }
    }
    
    return r;
  }

 /**
  * This is called when a data record with errors is encountered. You should do
  * any processing here that you have to do for error records, e.g. stratistics,
  * special handling, even error correction!
  */
  @Override
  public IRecord procErrorRecord(IRecord r)
  {
    return r;
  }
}

