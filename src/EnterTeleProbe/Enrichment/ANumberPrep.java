/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Ltd 2006-2012.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of arhitecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are discplaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
/* ========================== VERSION HISTORY =========================
 * $Log: ANumberPrep.java,v $
 * Revision 1.4  2012-07-25 16:59:03  jean-baptiste
 * new time rounding + agg
 *
 * Revision 1.3  2012-07-17 16:37:01  jean-baptiste
 * New date calculation and A number modification
 *
 * Revision 1.2  2012-03-24 14:32:55  ian
 * Add obfuscation, minor improvements
 *
 * Revision 1.1  2012/01/26 09:13:27  ian
 * Update for filtering
 *
 * ====================================================================
 */
package EnterTeleProbe.Enrichment;

import EnterTeleProbe.ProbeRecord;
import OpenRate.exception.ProcessingException;
import OpenRate.process.AbstractStubPlugIn;
import OpenRate.record.ErrorType;
import OpenRate.record.IRecord;
import OpenRate.record.RecordError;

/**
 * This module prepares the A Number for filtering.
 * 
 * @author ian
 */
public class ANumberPrep extends AbstractStubPlugIn
{
  // this is the CVS version info
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: ANumberPrep.java,v $, $Revision: 1.4 $, $Date: 2012-07-25 16:59:03 $";
    
 /**
  * This is called when a data record is encountered. You should do any normal
  * processing here.
  *
  * @param r The record we are working on
  * @return The processed record
  * @throws ProcessingException
  */
  @Override
  public IRecord procValidRecord(IRecord r) throws ProcessingException
  {
    ProbeRecord CurrentRecord = (ProbeRecord)r;

    if (CurrentRecord.RECORD_TYPE == ProbeRecord.PROBE_RECORD) 
    {
      // Deal with rogue records that do not have a called number
    /*  if ((CurrentRecord.callingNumber == null) ||
          (CurrentRecord.callingNumber.isEmpty()) ||
          (CurrentRecord.callingNumber.length() < 6))
      {
        // Add the error that we could not perform the normalisation
        RecordError tmpError = new RecordError("ERR_CALLING_NUMBER_INVALID", ErrorType.SPECIAL);
        CurrentRecord.addError(tmpError);
      }
      else
      {*/
        // Set the normalised A Number
        CurrentRecord.normCallingNumber = CurrentRecord.callingNumber;//.substring(CurrentRecord.callingNumber.length() - 6, CurrentRecord.callingNumber.length());

        // Copy to the value we use for matching. We might overwrite this later
        // if obfusction has been turned on.
        CurrentRecord.matchCallingNumber = CurrentRecord.normCallingNumber;
      //}
    }
    
    return r;
  }

 /**
  * This is called when a data record with errors is encountered. You should do
  * any processing here that you have to do for error records, e.g. stratistics,
  * special handling, even error correction!
  *
  * @param r The record we are working on
  * @return The processed record
  * @throws ProcessingException
  */
  @Override
  public IRecord procErrorRecord(IRecord r) throws ProcessingException
  {
    return r;
  }
  
}