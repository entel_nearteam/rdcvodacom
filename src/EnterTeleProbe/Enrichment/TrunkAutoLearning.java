/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Ltd 2006-2010.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of architecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are disclaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
/* ========================== VERSION HISTORY =========================
 * $Log: TrunkAutoLearning.java,v $
 * Revision 1.2  2014/07/31 16:11:06  florent
 * Auto learning
 *
 * Revision 1.1  2014/06/30 12:47:54  florent
 * Optimisation BDD
 * Auto aprentissage Tronc + filename
 *
 * Revision 1.3  2012-07-12 15:40:53  ian
 * Port lookup for Tpx, timestamp rounding
 *
 * Revision 1.2  2012-07-12 12:48:05  ian
 * timestamp sync
 *
 * Revision 1.1  2012-07-07 15:59:04  ian
 * Relate tables, add timestamp rounding, refactor
 *
 * Revision 1.2  2012-06-24 22:12:00  ian
 * Rating WIP
 *
 * ====================================================================
 */
package EnterTeleProbe.Enrichment;

import EnterTeleProbe.ProbeRecord;
import OpenRate.db.DBUtil;
import OpenRate.exception.InitializationException;
import OpenRate.exception.ProcessingException;
import OpenRate.process.AbstractStubPlugIn;
import OpenRate.record.IRecord;
import OpenRate.utils.ConversionUtils;
import OpenRate.utils.PropertyUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * This module calculates the time sync based on sync records, recognised both
 * in the probe and in the call layer. Based on this we are able to calculate
 * the effectice time offset.
 * 
 * The offset is applied to the Probe Record, based on a stream of sync records
 * that are known by the hash value in the table DESTINATION_INFO. If the
 * record is known, and is marked as a Y, we will treat it as a synch record.
 * If we have no synch informatio, we round the timestamps to the nearest
 * 15 seconds in any case, and the offset is calculated from these rounded
 * values to deal with large offsets.
 * 
 * @author TGDSPIA1
 */
public class TrunkAutoLearning extends AbstractStubPlugIn
{

  // Conversion object
  private static ConversionUtils conv;

  // this is our datasource object
  private String dataSourceName = null;

  // This is our connection object
  private Connection JDBCcon;

  // these are the statements that we have to prepare to be able to get
  // the records
  private static String SelectStatement;

  // these are the prepared statements
  private static PreparedStatement SelectStatementQuery;

  // Used for tracking 
  private class TrunkEntry
  {
    String newTrunkOperator = "";
    String newTrunk         = "";
    String newTrunkID       = "";
  }

  // This is the list of red records we have seen
  private static Map<String,TrunkEntry> trunkList = new TreeMap<String,TrunkEntry>(); 
  private static Hashtable tmpEntryArray = new Hashtable();


  // If we have loaded the table from the DB
  private static boolean loaded = false;
  
 /**
  * Initialise the module. Called during pipeline creation.
  *
  * @param PipelineName The name of the pipeline this module is in
  * @param ModuleName The name of this module in the pipeline
  * @throws OpenRate.exception.InitializationException
  */
  @Override
  public void init(String PipelineName, String ModuleName)
            throws InitializationException
  {
    // do standard initialisation
    super.init(PipelineName, ModuleName);
    
    // Get our context name
    conv = ConversionUtils.getConversionUtilsObject();
    conv.setOutputDateFormat("yyyy-MM-dd HH:mm:ss");
    
    if (loaded == false)
    {
      // Get the information from the DB
      // Get the data source name
      dataSourceName = PropertyUtils.getPropertyUtils().getPluginPropertyValueDef(PipelineName, 
                                                                                  ModuleName,
                                                                                  "DataSource",
                                                                                  "None");

      if (dataSourceName.equalsIgnoreCase("None"))
      {
        getPipeLog().error(
              "Data source DB name not found for cache <" + getSymbolicName() +
              ">");
        throw new InitializationException("Data source DB name not found for cache <" +
                                          getSymbolicName() + ">",getSymbolicName());
      }
      else
      {
        getPipeLog().debug(
              "Found time sync DB:" + dataSourceName);
      }

      // Get the insert statement
      SelectStatement = PropertyUtils.getPropertyUtils().getPluginPropertyValueDef(PipelineName, 
                                                                                  ModuleName,
                                                                                  "SelectStatement",
                                                                                  "None");

      if (SelectStatement.equals("None"))
      {
        getPipeLog().error("Key insert statement not found for cache <" + getSymbolicName() +
                  ">");
        throw new InitializationException("Key insert statement not found for cache <" +
                                          getSymbolicName() + ">",getSymbolicName());
      }
      else
      {
        getPipeLog().debug(
              "Found Insert Statement <" + SelectStatement +
              "> for cache <" + getSymbolicName() + ">");
      }

      // The datasource property was added to allow database to database
      // JDBC adapters to work properly using 1 configuration file.
      DBUtil.initDataSource(dataSourceName);

      // Prepare the statments
      try
      {
        // Try to open the DS
        JDBCcon = DBUtil.getConnection(dataSourceName);
      }
      catch (Exception ex)
      {
        String Message = "Error getting the connection the statement <" + dataSourceName + ">";
        throw new InitializationException(Message, ex,getSymbolicName());
      }

      // Prepare the statments
      try
      {
        // prepare the SQL for the InsertStatement
        SelectStatementQuery = JDBCcon.prepareStatement(SelectStatement,
                                                      ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                      ResultSet.CLOSE_CURSORS_AT_COMMIT);
      }
      catch (SQLException ex)
      {
        getPipeLog().error("Error preparing the statement <" + SelectStatement + ">");
        throw new InitializationException("Error preparing the statement <" +
                                          SelectStatement + ">",getSymbolicName());
      }
      catch (NullPointerException npe)
      {
        getPipeLog().error("Error preparing the statement <" + SelectStatement + ">");
        throw new InitializationException("Error preparing the statement <" +
                                          SelectStatement + ">", npe,getSymbolicName());
      }

      try
      {
        // Try the insert
        SelectStatementQuery.clearParameters();
        ResultSet rs = SelectStatementQuery.executeQuery();


        // Now load the arraylist
        trunkList.clear();

        rs.beforeFirst();

        // For each operator, get data
              rs.beforeFirst();
              while (rs.next())
              {
         
                    TrunkEntry newEntry       = new TrunkEntry();
                    newEntry.newTrunk         = rs.getString(1);
                    newEntry.newTrunkOperator = rs.getString(2);
                    newEntry.newTrunkID       = rs.getString(3);
                    
                    trunkList.put(rs.getString(1),newEntry);
              }
        
        rs.close();
        SelectStatementQuery.close();
        JDBCcon.close();
      } catch (SQLException ex)
      {
        String Message = "Error getting cache information";
        throw new InitializationException(Message,ex,getSymbolicName());
      }
      
      // say that we are loaded
      loaded = true;
    }
  }
  
 /**
  * This is called when a data record is encountered. You should do any normal
  * processing here.
  *
  * @param r The record we are working on
  * @return The processed record
  * @throws ProcessingException
  
  */
  @Override
  public IRecord procValidRecord(IRecord r) throws ProcessingException
  {
    boolean foundMatch = false;
    ProbeRecord CurrentRecord = (ProbeRecord) r;
                //check if trunk list contains CDR In trunk
              if (trunkList.containsKey(CurrentRecord.inTrunkId))
              {
                  TrunkEntry tmpEntry       =   trunkList.get(CurrentRecord.inTrunkId);
                  //set trunk ID for DB index
                  CurrentRecord.inTrunkId   =   tmpEntry.newTrunkID;
                  // say that we don't have to add the entry
                  foundMatch = true;
              }
              else
              {
                  //create new trunk to store in cache tab
                  TrunkEntry newTrunkToAdd          =   new TrunkEntry();
                  newTrunkToAdd.newTrunkOperator    =   CurrentRecord.mapGroup;
                  newTrunkToAdd.newTrunk            =   CurrentRecord.inTrunkId;
                  newTrunkToAdd.newTrunkID          =   Integer.toString(trunkList.size()+1); //incremente ID
                  trunkList.put(CurrentRecord.inTrunkId,newTrunkToAdd);
                  
                  CurrentRecord.inTrunkName         =   CurrentRecord.inTrunkId;
                  CurrentRecord.inTrunkId           =   newTrunkToAdd.newTrunkID;
                  CurrentRecord.usesNewTrunk        =   10; //utilisé comme nombre binaire (00,01,10,11) avec 1er digit= in_trunk et 2nd digit =out_trunk
                  
              }
              if (trunkList.containsKey(CurrentRecord.outTrunkId))
              {
                  TrunkEntry tmpEntry       =   trunkList.get(CurrentRecord.outTrunkId);
                  //set trunk ID for DB index
                  CurrentRecord.outTrunkId   =   tmpEntry.newTrunkID;
                  // say that we don't have to add the entry
                  foundMatch = true;
              }
              else
              {
                  //create new trunk to store in cache tab
                  TrunkEntry newTrunkToAdd          =   new TrunkEntry();
                  newTrunkToAdd.newTrunkOperator    =   CurrentRecord.mapGroup;
                  newTrunkToAdd.newTrunk            =   CurrentRecord.outTrunkId;
                  newTrunkToAdd.newTrunkID          =   Integer.toString(trunkList.size()+1); //incremente ID
                  trunkList.put(CurrentRecord.outTrunkId,newTrunkToAdd);
                  
                  CurrentRecord.outTrunkName         =   CurrentRecord.outTrunkId;
                  CurrentRecord.outTrunkId           =   newTrunkToAdd.newTrunkID;
                  CurrentRecord.usesNewTrunk++; //utilisé comme nombre binaire (00,01,10,11) avec 1er digit= in_trunk et 2nd digit =out_trunk
              }
              if ((CurrentRecord.usesNewTrunk == 1) || (CurrentRecord.usesNewTrunk == 10))
              {
                  //Call procedure to save in DB
                  CurrentRecord.addOutput("1TrunkOutput");
              }
              else if (CurrentRecord.usesNewTrunk == 11)
              {
                  //Call procedure to save in DB
                   CurrentRecord.addOutput("2TrunksOutput");
              }
    return r;
  }

 /**
  * This is called when a data record with errors is encountered. You should do
  * any processing here that you have to do for error records, e.g. stratistics,
  * special handling, even error correction!
  *
  * @param r The record we are working on
  * @return The processed record
  * @throws ProcessingException
  */
  @Override
  public IRecord procErrorRecord(IRecord r) throws ProcessingException
  {
    // Do nothing
    return r;
  }

 /**
  * Create a string which can be loaded into the DB for persistence
  * 
  * @param tmpEntry The entry we want to convert
  * @return The separated string value
  */
//  private String createTrunkRecord(TimestampEntry tmpEntry)
//  {
//    String newSyncRecord;
//    
//    // Add the record for output
//    newSyncRecord = tmpEntry.destID + ":" +
//                    tmpEntry.callLayerTime + ":" +
//                    tmpEntry.cdrTime + ":" +
//                    tmpEntry.offset;
//    
//    return newSyncRecord;
//  }
}
