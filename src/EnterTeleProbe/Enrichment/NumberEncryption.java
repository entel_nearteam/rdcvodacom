/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Ltd 2006-2012.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of arhitecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are discplaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
/* ========================== VERSION HISTORY =========================
 * $Log: NumberEncryption.java,v $
 * Revision 1.2  2014/07/31 16:11:06  florent
 * Auto learning
 *
 * Revision 1.2  2012-07-17 16:37:01  jean-baptiste
 * New date calculation and A number modification
 *
 * Revision 1.1  2012-03-24 14:32:55  ian
 * Add obfuscation, minor improvements
 *
 * ====================================================================
 */
package EnterTeleProbe.Enrichment;

import EnterTeleProbe.ProbeRecord;
import OpenRate.exception.InitializationException;
import OpenRate.exception.ProcessingException;
import OpenRate.process.AbstractStubPlugIn;
import OpenRate.record.IRecord;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This module obfuscates the the A Number and B Number for filtering using the
 * MD5 one way hash (message digest) algorithm.
 * 
 * @author ian
 */
public class NumberEncryption extends AbstractStubPlugIn
{
  // this is the CVS version info
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: NumberEncryption.java,v $, $Revision: 1.2 $, $Date: 2014/07/31 16:11:06 $";
  
  // Shared message digest object
  private static MessageDigest md5Digest;
  // Private key for md5
  private String MD5secret = "track92CDR975iYR4Lyr";
  private String tmpData;
  
 /**
  * Initialise the module. Called during pipeline creation.
  *
  * @param PipelineName The name of the pipeline this module is in
  * @param ModuleName The name of this module in the pipeline
  * @throws OpenRate.exception.InitializationException
  */
  @Override
  public void init(String PipelineName, String ModuleName)
            throws InitializationException
  {
    // Perform super initialisation
    super.init(PipelineName, ModuleName);
    try 
    {
    	System.out.println("NE. PipelineName : " + PipelineName);
    	System.out.println("NE. ModuleName : " + ModuleName);
      // Get the message digest object
      md5Digest = MessageDigest.getInstance("MD5");
    } 
    catch (NoSuchAlgorithmException ex) 
    {
      throw new InitializationException("MD5 Algorithm not known",getSymbolicName());
    }
  }
  
 /**
  * This is called when a data record is encountered. You should do any normal
  * processing here.
  *
  * @param r The record we are working on
  * @return The processed record
  * @throws ProcessingException
  */
  @Override
  public IRecord procValidRecord(IRecord r) throws ProcessingException
  {
    byte[] data;
    BigInteger digestValue;
    
    ProbeRecord CurrentRecord = (ProbeRecord)r;

    if (CurrentRecord.RECORD_TYPE == ProbeRecord.PROBE_RECORD)
    {
  //    md5Digest.reset();
     //data = CurrentRecord.normCallingNumber.getBytes(); 
  //    md5Digest.update(data,0,data.length);
  //    digestValue = new BigInteger(1,md5Digest.digest());
      CurrentRecord.matchCallingNumber = CurrentRecord.normCallingNumber; //String.format("%1$032X", digestValue).toLowerCase();
      try
      {
    	  
      md5Digest.reset();
      tmpData=CurrentRecord.normCalledNumber + MD5secret;
      data = tmpData.getBytes(); 
      md5Digest.update(data,0,data.length);
      System.out.println("md5Digest tostring : " + md5Digest.toString());
      digestValue = new BigInteger(1,md5Digest.digest());
      CurrentRecord.matchCalledNumber = String.format("%1$032X", digestValue).toLowerCase();
      }
      catch(Exception e)
      {
    	  e.printStackTrace();
      }
    }    
    
    return r;
  }

 /**
  * This is called when a data record with errors is encountered. You should do
  * any processing here that you have to do for error records, e.g. stratistics,
  * special handling, even error correction!
  *
  * @param r The record we are working on
  * @return The processed record
  * @throws ProcessingException
  */
  @Override
  public IRecord procErrorRecord(IRecord r) throws ProcessingException
  {
    return r;
  }
  
}