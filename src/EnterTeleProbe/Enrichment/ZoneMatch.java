/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Ltd 2006-2010.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of architecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are disclaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherA0111675_102010_261023.out.donewise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
/* ========================== VERSION HISTORY =========================
 * $Log: ZoneMatch.java,v $
 * Revision 1.3  2012-03-24 14:32:55  ian
 * Add obfuscation, minor improvements
 *
 * Revision 1.2  2012/01/19 17:23:02  ian
 * Add aggregation and CDR output
 *
 * Revision 1.1  2012/01/18 12:35:56  ian
 * Add normlisation and zoning
 *
 * ====================================================================
 */
package EnterTeleProbe.Enrichment;

import OpenRate.exception.ProcessingException;
import EnterTeleProbe.AbstractRUMBestMatch2;
import EnterTeleProbe.ProbeRecord;
import OpenRate.record.*;
import java.util.ArrayList;

/**
 * This class performs the best match lookup on the non C-Type numbers, giving
 * the correct TIER-CODE value to allow rating. This only happens for records
 * that have been identified as not having a C-prefix number. Thus this module
 * and the "OperatorNumberMatch" are mutually exclusive.
 *
 * @author afzaal
 */


public class ZoneMatch extends AbstractRUMBestMatch2
{
  /**
   * CVS version info - Automatically captured and written to the Framework
   * Version Audit log at Framework startup. For more information
   * please <a target='new' href='http://www.open-rate.com/wiki/index.php?title=Framework_Version_Map'>click here</a> to go to wiki page.
   */
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: ZoneMatch.java,v $, $Revision: 1.3 $, $Date: 2012-03-24 14:32:55 $";

  // -----------------------------------------------------------------------------
  // ------------------ Start of inherited Plug In functions ---------------------
  // -----------------------------------------------------------------------------

  /**
  * This is called when a data record is encountered. You should do any normal
  * processing here.
  *
  * This transformation looks up the zone name prefix using the best match
  * ZoneCache lookup. Because this example does not care about services, we
  * define the service type as a default "DEF".
  */
  @Override
  public IRecord procValidRecord(IRecord r)
  {

    IError tmpError;
    ProbeRecord CurrentRecord = (ProbeRecord)r;

    // We only transform the ergatel records which are not C Type records, and all
    // Broadsoft records
    if (CurrentRecord.RECORD_TYPE == ProbeRecord.PROBE_RECORD)
    {
      // set the zone model
      CurrentRecord.getChargePacket(0).zoneModel = CurrentRecord.mapGroup;
      
try
      {
        performRUMBestMatchWithChildData2(CurrentRecord, CurrentRecord.normCalledNumber);
        CurrentRecord.destination   =   CurrentRecord.tmpDest;
        CurrentRecord.DestinationID =   CurrentRecord.tmpPref;
      }
      catch (ProcessingException pe)
      {
        
         CurrentRecord.destination   = "NULL"; 
         CurrentRecord.DestinationID = "NULL"; 
         
          
        //PipeLog.error("SBERR,ERR_ZONE_LOOKUP,0,"+CurrentRecord.normCalledNumber+","+CurrentRecord.EventStartDate);
        //tmpError = new RecordError("ERR_ZONE_LOOKUP", ErrorType.SPECIAL,CurrentRecord.normCalledNumber);
        //CurrentRecord.addError(tmpError);
      }
      
        try
      {
        performRUMBestMatchWithChildData2(CurrentRecord, CurrentRecord.callingNumber);
        CurrentRecord.Origin   =   CurrentRecord.tmpDest;
        CurrentRecord.OriginID =   CurrentRecord.tmpPref;
      }
      catch (ProcessingException pe)
      {
        
         CurrentRecord.Origin   = "NULL"; 
         CurrentRecord.OriginID = "NULL"; 
          
        //PipeLog.error("SBERR,ERR_ZONE_LOOKUP,0,"+CurrentRecord.normCalledNumber+","+CurrentRecord.EventStartDate);
        //tmpError = new RecordError("ERR_ZONE_LOOKUP", ErrorType.SPECIAL,CurrentRecord.normCalledNumber);
        //CurrentRecord.addError(tmpError);
      }
    }
      
    return r;
  }

  /**
  * This is called when a data record with errors is encountered. You should do
  * any processing here that you have to do for error records, e.g. statistics,
  * special handling, even error correction!
  */
  @Override
  public IRecord procErrorRecord(IRecord r)
  {
    return r;
  }

 /**
  * This method overwrites the standard method for filling the charge packets.
  * In addition to setting the Zone Result and the Zone Description, it also
  * sets the category on the record.
  *
  * @param tmpCP The Charge Packet we are working on
  * @param BestMatchZone The Best match data vector
  */
  @Override
  protected void fillCPWithBestMatchChildData(RatingRecord CurrentRecord, ChargePacket tmpCP, ArrayList<String> BestMatchZone)
  {
    ProbeRecord tmpRecord = (ProbeRecord)CurrentRecord;

    if (BestMatchZone.size() == 4)
    {
      tmpCP.zoneResult = BestMatchZone.get(0);
      tmpCP.zoneInfo = BestMatchZone.get(1);
      tmpRecord.tmpDest = BestMatchZone.get(1);
      tmpRecord.tmpPref = BestMatchZone.get(3);
      tmpRecord.category = BestMatchZone.get(2);
    }
  }
}
