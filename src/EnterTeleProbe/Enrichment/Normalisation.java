/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Limited 2006-2012.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of architecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are discplaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
/* ========================== VERSION HISTORY =========================
 * $Log: Normalisation.java,v $
 * Revision 1.2  2012-03-24 14:32:55  ian
 * Add obfuscation, minor improvements
 *
 * Revision 1.1  2012/01/18 12:35:56  ian
 * Add normlisation and zoning
 *
 * Revision 1.1  2012/01/17 12:54:51  ian
 * Add trunk mapping
 *
 * ====================================================================
 */
package EnterTeleProbe.Enrichment;

import EnterTeleProbe.ProbeRecord;
import OpenRate.process.AbstractRegexMatch;
import OpenRate.record.ErrorType;
import OpenRate.record.IRecord;
import OpenRate.record.RecordError;
import java.util.ArrayList;

/**
 * This class is used to normalize the Called Number. It also sets the values
 * into the matchCalled/Calling Number. If there is obfusction in the pipeline,
 * these will be overwritten later with the MD5 hash version.
 */
public class Normalisation
  extends AbstractRegexMatch
{
  // this is the CVS version info
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: Normalisation.java,v $, $Revision: 1.2 $, $Date: 2012-03-24 14:32:55 $";

  String[] tmpSearchParameters = new String[2];
  
  // -----------------------------------------------------------------------------
  // ------------------ Start of inherited Plug In functions ---------------------
  // -----------------------------------------------------------------------------

 /**
  * This is called when a data record is encountered. You should do any normal
  * processing here.
  */
  @Override
  public IRecord procValidRecord(IRecord r)
  {
    ArrayList<String> Results;
    ProbeRecord CurrentRecord = (ProbeRecord) r;

    if (CurrentRecord.RECORD_TYPE == ProbeRecord.PROBE_RECORD)
    {
      // Deal with rogue records that do not have a called number
      if ((CurrentRecord.calledNumber == null) || (CurrentRecord.calledNumber.isEmpty()))
      {
        // Add the error that we could not perform the normalisation
        RecordError tmpError = new RecordError("ERR_CALLED_NUMBER_INVALID", ErrorType.SPECIAL);
        CurrentRecord.addError(tmpError);
      }
      else
      {
        tmpSearchParameters[0] = "";                          // Band currently not used
        tmpSearchParameters[1] = CurrentRecord.calledNumber;

        // Perform the lookup
        Results = getRegexMatchWithChildData(CurrentRecord.mapGroup,tmpSearchParameters);
         if (!isValidRegexMatchResult(Results)) Results = getRegexMatchWithChildData("Default",tmpSearchParameters);
        // Check we have some results
        if (isValidRegexMatchResult(Results))
        {
          if (Results.get(0).isEmpty())
          {
            // just add the prefix
            CurrentRecord.normCalledNumber = Results.get(1) + CurrentRecord.calledNumber;

            // Copy to the value we use for matching. We might overwrite this later
            // if obfusction has been turned on.
            CurrentRecord.matchCalledNumber = CurrentRecord.normCalledNumber;
          }
          else
          {
            // remove an old prefix and add the new prefix
            CurrentRecord.normCalledNumber = CurrentRecord.calledNumber.replaceAll(Results.get(0), Results.get(1));

            // Copy to the value we use for matching. We might overwrite this later
            // if obfusction has been turned on.
            CurrentRecord.matchCalledNumber = CurrentRecord.normCalledNumber;
          }
        }
        else
        {
          // if obfusction has been turned on.
          // Copy to the value we use for matching. We might overwrite this later
          CurrentRecord.matchCalledNumber = CurrentRecord.normCalledNumber;

          // Add the error that we could not perform the normalisation
          RecordError tmpError = new RecordError("ERR_NORMALISATION_LOOKUP", ErrorType.SPECIAL);
          CurrentRecord.addError(tmpError);
        }
      }
    }
    
    return r;
  }

 /**
  * This is called when a data record with errors is encountered. You should do
  * any processing here that you have to do for error records, e.g. stratistics,
  * special handling, even error correction!
  */
  @Override
  public IRecord procErrorRecord(IRecord r)
  {
    return r;
  }
}