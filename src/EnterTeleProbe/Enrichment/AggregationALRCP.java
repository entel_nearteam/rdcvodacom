/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Ltd 2006-2012.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of arhitecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are discplaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
/* ========================== VERSION HISTORY =========================
 * $Log: Aggregation.java,v $
 * Revision 1.9  2012-07-12 06:01:37  ian
 * Better date management
 *
 * Revision 1.8  2012-06-18 15:58:25  jean-baptiste
 * Added Operator info in aggregation
 * Needed to know which probe the cdr comes from
 *
 * Revision 1.7  2012-06-13 08:48:54  jean-baptiste
 * added Pending Duration Delay enrichment
 *
 * Revision 1.6  2012-04-04 17:19:36  ian
 * Add Connection Count indicator for ASR
 *
 * Revision 1.5  2012/03/24 14:32:55  ian
 * Add obfuscation, minor improvements
 *
 * Revision 1.4  2012/01/26 09:13:27  ian
 * Update for filtering
 *
 * Revision 1.3  2012/01/19 17:23:02  ian
 * Add aggregation and CDR output
 *
 * Revision 1.2  2012/01/17 11:55:13  ian
 * Map CS and IGW types
 *
 * Revision 1.1  2012/01/16 22:51:53  ian
 * Initial Starting Version
 *
 * ====================================================================
 */
package EnterTeleProbe.Enrichment;

import EnterTeleProbe.ProbeRecord;
import OpenRate.exception.InitializationException;
import OpenRate.exception.ProcessingException;
import OpenRate.process.AbstractAggregation;
import OpenRate.record.ErrorType;
import OpenRate.record.HeaderRecord;
import OpenRate.record.IRecord;
import OpenRate.record.RecordError;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * 
 */
public class AggregationALRCP
    extends AbstractAggregation
{
  // this is the CVS version info
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: Aggregation.java,v $, $Revision: 1.9 $, $Date: 2012-07-12 06:01:37 $";

  // The aggregation fields
  private String[] AggFields = new String[14];

  // The Aggregation filter we are using for good records
  private ArrayList<String> defaultAggFilter = new ArrayList<String>();
  
  // The Aggregation filter we are using for errored records
  private ArrayList<String> errorAggFilter = new ArrayList<String>();
  
  // The base name of the file
  private String baseName;
  
  // -----------------------------------------------------------------------------
  // ------------------ Start of inherited Plug In functions ---------------------
  // -----------------------------------------------------------------------------

  @Override
  public void init(String PipelineName, String ModuleName)
            throws InitializationException
  {
    // perform parent init
    super.init(PipelineName, ModuleName);
    
    // init our agg filter for good records
    defaultAggFilter.add("Aggregate");
  }
  
  /**
  * This is called when a data record is encountered. You should do any normal
  * processing here.
  */
  @Override
  public IRecord procHeader(IRecord r)
  {
    HeaderRecord headerRec = (HeaderRecord) r;
   
    // call the original processing
    super.procHeader(r);
    
    // Get the base name
    baseName = headerRec.getStreamName();
    
    return r;
  }
    
  /**
  * This is called when a data record is encountered. You should do any normal
  * processing here.
  */
    @Override
  public IRecord procValidRecord(IRecord r)
  {
    ProbeRecord CurrentRecord = (ProbeRecord)r;

    if ((CurrentRecord.RECORD_TYPE == ProbeRecord.PROBE_RECORD) || (CurrentRecord.RECORD_TYPE == ProbeRecord.PROFILE_RECORD) || (CurrentRecord.RECORD_TYPE == ProbeRecord.LIVE_TRACK_RECORD))
    {
      // the field that will be used for the aggregation scenario
      AggFields[0] = CurrentRecord.mapGroup.replaceAll("Profile", "");
      AggFields[1] = CurrentRecord.recordDate;
      AggFields[2] = CurrentRecord.roundedDate5;
      AggFields[3] = CurrentRecord.inTrunkId;
      AggFields[4] = CurrentRecord.inOperator;
      AggFields[5] = CurrentRecord.outTrunkId;
      AggFields[6] = CurrentRecord.outOperator;
      AggFields[7] = CurrentRecord.CallType;
      AggFields[8] = CurrentRecord.trunkType;
      AggFields[9] = "";
      AggFields[10] = CurrentRecord.destination;
      AggFields[11] = Long.toString(CurrentRecord.duration);
      AggFields[12] = Long.toString(CurrentRecord.PDD);
      AggFields[13] = Long.toString(CurrentRecord.callConnected);
      
      // perform the aggregation
      if (!CurrentRecord.secondRecord)
      {
          try
          {
            Aggregate(AggFields,defaultAggFilter);
          }
          catch (ProcessingException pe)
          {
            RecordError tmpError = new RecordError("ERR_AGGREGATION_FAILED",ErrorType.SPECIAL);
            tmpError.setModuleName(getSymbolicName());
            tmpError.setErrorDescription("Aggregation error:"+AggFields[0]+":"+AggFields[1]+":"+AggFields[2]+":"+AggFields[3]+":"+AggFields[4]+":"+AggFields[5]+":"+AggFields[6]+":"+AggFields[7]);
            CurrentRecord.addError(tmpError);
          }
      }
    
      // if profiled number, change aggregated field
      if (CurrentRecord.RECORD_TYPE == ProbeRecord.PROFILE_RECORD)
      {
           AggFields[0] = CurrentRecord.mapGroup;
           AggFields[7] = CurrentRecord.profileType; 
           AggFields[8] = CurrentRecord.profiledNumber; 
          try
          {
            Aggregate(AggFields,defaultAggFilter);
          }
          catch (ProcessingException pe)
          {
            RecordError tmpError = new RecordError("ERR_AGGREGATION_FAILED",ErrorType.SPECIAL);
            tmpError.setModuleName(getSymbolicName());
            tmpError.setErrorDescription("Aggregation error:"+AggFields[0]+":"+AggFields[1]+":"+AggFields[2]+":"+AggFields[3]+":"+AggFields[4]+":"+AggFields[5]+":"+AggFields[6]+":"+AggFields[7]);
            CurrentRecord.addError(tmpError);
          }
      }
      
   
    }
    return r;
  }

  /**
  * This is called when a data record with errors is encountered. You should do
  * any processing here that you have to do for error records, e.g. stratistics,
  * special handling, even error correction!
  */
  @Override
  public IRecord procErrorRecord(IRecord r)
  {
    ProbeRecord CurrentRecord = (ProbeRecord)r;

    if (CurrentRecord.RECORD_TYPE == ProbeRecord.PROBE_RECORD)
    {
      for (int i = 0 ; i < CurrentRecord.getErrorCount() ; i++)
      {
        // the field that will be used for the aggregation scenario
        AggFields[0] = CurrentRecord.fileName;
        AggFields[1] = CurrentRecord.connectTime;
        AggFields[2] = CurrentRecord.getErrors().get(i).getMessage();
        AggFields[3] = Double.toString(0);
        AggFields[4] = Long.toString(CurrentRecord.duration);

        // perform the aggregation
        try
        {
          // Defaulted to not perform aggregation!!! ErrorAggFilter is empty.
          Aggregate(AggFields,errorAggFilter);
        }
        catch (ProcessingException pe)
        {
          RecordError tmpError = new RecordError("ERR_AGGREGATION_FAILED",ErrorType.SPECIAL);
          tmpError.setModuleName(getSymbolicName());
          tmpError.setErrorDescription("Aggregation error:"+AggFields[0]+":"+AggFields[1]+":"+AggFields[2]+":"+AggFields[3]+":"+AggFields[4]);
          CurrentRecord.addError(tmpError);
        }
      }
    }
    
    return r;
  }
  
 /**
  * give us a better naming for the aggregation files than the transaction alone
  *
  * @param TransactionNumber The transaction to get the file base name for
  * @return The unmodified header record
  */
  @Override
  public String getAggregationFileBaseName(int TransactionNumber)
  {
    
    return "Agg-ALRCP-" + baseName + "-" + Calendar.getInstance().getTimeInMillis();
  }
}
