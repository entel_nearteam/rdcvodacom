/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EnterTeleProbe.Decoding;
import java.util.ArrayList;

import OpenRate.adapter.file.BinaryFileInputAdapter;
import OpenRate.parser.ASN1Parser;
import OpenRate.parser.Asn1Class;
import OpenRate.record.FlatRecord;
import OpenRate.record.IRecord;
import OpenRate.record.RatingRecord;


/**
 *
 * @author hmarikan
 */
public class StandardTelASN1InputAdatpter extends BinaryFileInputAdapter {
    


/**
 * Binary input file adapter. As the files are not that large, we attempt to
 * parse them all in one go. This means that we load the entire binary file
 * contents into memory and then run through it decoding. This gives us a huge
 * performance boost over random access reading.
 */
  // this is the CVS version info
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: BalInputAdapter.java,v $, $Revision: 1.1 $, $Date: 2012/01/29 13:31:51 $";

  // This is the object that is used to compress the records
  RatingRecord tmpDataRecord = null;

  // Defintion of the tags and so on
  StandardTelDef asn1Specification = new StandardTelDef();
  
  // our ASN1 parser object
  ASN1Parser asnp;
  
  // -----------------------------------------------------------------------------
  // ------------------ Start of inherited Plug In functions ---------------------
  // -----------------------------------------------------------------------------

 /**
  * This is called when the synthetic Header record is encountered, and has the
  * meaning that the stream is starting. In this example we have nothing to do
  * 
  * @param r The record we are working on
  * @return The processed record
  */
  @Override
  public IRecord procHeader(IRecord r)
  {
    asn1Specification.initTags();
    asnp = new ASN1Parser(asn1Specification);
  
    return r;
  }

 /**
  * This is called when a data record is encountered. You should do any normal
  * processing here. For the input adapter, we probably want to change the 
  * record type from FlatRecord to the record(s) type that we will be using in
  * the processing pipeline.
  *
  * This is also the location for accumulating records into logical groups
  * (that is records with sub records) and placing them in the pipeline as
  * they are completed. If you receive a sub record, simply return a null record
  * in this method to indicate that you are handling it, and that it will be
  * purged at a later date.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procValidRecord(IRecord r)
  {
    return r;
  }

 /**
  * This is called when a data record with errors is encountered. You should do
  * any processing here that you have to do for error records, e.g. statistics,
  * special handling, even error correction!
  * 
  * The input adapter is not expected to provide any records here.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procErrorRecord(IRecord r)
  {
    return r;
  }

 /**
  * This is called when the synthetic trailer record is encountered, and has the
  * meaning that the stream is now finished. In this example, all we do is 
  * pass the control back to the transactional layer.
  *
  * In models where record aggregation (records and sub records) is used, you
  * might want to check for any purged records here.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procTrailer(IRecord r)
  {
    return r;
  }

 /**
  * This is where the binary parsing happens.
  * 
  * @param fileContents The contents of the binary file
  * @return An array of records to push into the pipe
  */
  @Override
  public ArrayList<IRecord> parseBinaryFileContents(byte[] fileContents) {
    ArrayList<IRecord> outBatch = new ArrayList<>();
   //System.out.println("File Length  : " + fileContents.length);
    // Current asn1 element we are working on
    Asn1Class output;
    int cdrCounter = 0;
    // Set up the data to parse
    asnp.setDataToParse(fileContents);
    
    
    while(asnp.ready()) {
     try {
         byte[] cdrBlocks = asnp.readBlock(250); 
         IRecord record = processCDR(cdrBlocks);
        record.setRecordID(cdrCounter++);
         outBatch.add(record);
                
     } catch (Exception ex) {
         
     }
        
    }
        System.out.println("Porcessed  : " + cdrCounter + " CDRs ");
        return outBatch;
  }
  
 /**
  * process the Header block.
  * 
  * @param cdrBlock the CDR block to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private int processHeaderBlock(byte[] headerBlock) throws Exception
  {
    Asn1Class output;
    
    return 0;
  }
  
 /**
  * process the Trailer block.
  * 
  * @param cdrBlock the CDR block to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private int processTrailerBlock(byte[] trailerBlock) throws Exception
  {
    Asn1Class output;
    
    return 0;
  }
  

 /**
  * process the CDR.
  * 
  * @param cdrRecord the CDR to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private IRecord processCDR(byte[] cdrRecord) throws Exception
  {
    Asn1Class output;
    StringBuilder recordContents = new StringBuilder();
    ASN1Parser asnpr = new ASN1Parser(asn1Specification);
    int cdrType = 0;
    String callType; 
    //System.out.println("processing CDR\n");
       
    
    // Set up the data for the record
    asnpr.setDataToParse(cdrRecord);
      
       byte [] cdrHeader = asnpr.readBlock(12);
       byte [] buffer = asnpr.readBlock(1);
       cdrType  |= (0x000000FF & buffer[0]);
    
       recordContents.append("{").append("Read CDR type").append("=").append(cdrType).append("};");
     
       switch (cdrType) {
           case 0:
               callType = "MOC";
               break;
           case 1:
                callType = "MTC";
               break;
           case 2:
                callType = "ROAM";
                break;
           case 3:
                callType = "EC";
                 break;
           case 4:
                callType = "TRANSIT";
               break;
           case 6:
                callType = "MT_SMS";
               break;  
               
           case 7:
                callType = "MO_SMS";
               break; 
           case 12:
                callType = "ISDN_MO";
               break;  
               
           case 13:
                callType = "ISDN_MT";
               break; 
               
           case 18:
                callType = "CFW";
               break;  
               
               
           case 20:
                callType = "PDSN_BILL";
               break;  
            
           default:
                callType = "OTHER";
               
           
       }
       
       
       
       //String cdrType = asnp.parseBytes(output.getOrigValue());
       recordContents.append("{").append("call Type").append("=").append(callType).append("};");
            // Calculate the tag type for the lookup out of CDR type and cdr field tag

       buffer = asnpr.readBlock(7);
       int year = 0;
       year  |= ((0x000000FF & buffer[1]) << 8)
                  | (0x000000FF & buffer[0]);
       
recordContents.append("{").append("Charge_start_time : Year").append("=").append(year).append("};");
     
       int month = 0;
       int day = 0;
       int hr = 0;
       int minute = 0;
       int sec = 0;
       
       month |= (0x000000FF & buffer[2]);
       
       day |= (0x000000FF & buffer[3]);
       
       hr |= (0x000000FF & buffer[2]);
       minute |= (0x000000FF & buffer[2]);
       
       sec |= (0x000000FF & buffer[2]);
       
              
     recordContents.append("{").append("year").append("=").append(year).append("month").append("=").append(month).append("date").append("=").append(day).append("hr").append("=").append(hr).append("minute").append("=").append(minute).append("sec").append("=").append(sec).append("};");
  
       //construct a date object
       
       buffer = asnpr.readBlock(18);
       byte[] newBuf = new byte[16];
       int i = 2;
       for(i = 2; i < 18; i++) {
           
           newBuf[i-2] = buffer[i];
              
        }
       
       
       String callerPartyNumber = asnpr.parseBCDString(newBuf);
       
        recordContents.append("{").append("callerPartyNumber").append("=").append(callerPartyNumber).append("};");
          
        buffer = asnpr.readBlock(18);
        for(i = 2; i < 18; i++) {
           
           newBuf[i-2] = buffer[i];
               
        }
       
        
       String calledPartyNumber = asnpr.parseBCDString(newBuf);
       
       recordContents.append("{").append("calledPartyNumber").append("=").append(calledPartyNumber).append("};");
        
       
       
        buffer = asnpr.readBlock(18);
        for(i = 2; i < 18; i++) {
           
           newBuf[i-2] = buffer[i];
             
        }
       
       String thirdPartyNumber = asnpr.parseBCDString(newBuf);
       
        recordContents.append("{").append("thirdPartyNumber").append("=").append(thirdPartyNumber).append("};");
      
      
        buffer = asnpr.readBlock(8);
       
       String calledTLDN = asnpr.parseBCDString(buffer);

       recordContents.append("{").append("called_TLDN").append("=").append(calledTLDN).append("};");
       
       
       buffer = asnpr.readBlock(8);
       
       String chargedMDN = asnpr.parseBCDString(buffer);
recordContents.append("{").append("charged_MDN").append("=").append(chargedMDN).append("};");
       

         buffer = asnpr.readBlock(8);
       
       String servedIMSI = asnpr.parseBCDString(buffer);
recordContents.append("{").append("served_IMSI").append("=").append(servedIMSI).append("};");
       
       buffer = asnpr.readBlock(4);
       
       String esn = asnpr.parseInteger(buffer);
 recordContents.append("{").append("served_ESN").append("=").append(esn).append("};");
      
         buffer = asnpr.readBlock(7);
       String billID = asnpr.parseInteger(buffer);
recordContents.append("{").append("billID").append("=").append(billID).append("};");

       buffer = asnpr.readBlock(4);
        
       
      
       buffer = asnpr.readBlock(4);
       int duration = 0;
       
       
      duration  |= ((0x000000FF & buffer[3]) << 24)
                  | (0x000000FF & buffer[1] << 16)
                   | (0x000000FF & buffer[2] << 8)
              | (0x000000FF & buffer[0]);
       
 recordContents.append("{").append("charge_duration").append("=").append(duration).append("};");
      
       
        buffer = asnpr.readBlock(4);
       
       int firstCDRIndex = 0;
       
      firstCDRIndex  |= ((0x000000FF & buffer[3]) << 24)
                  | (0x000000FF & buffer[1] << 16)
                   | (0x000000FF & buffer[2] << 8)
              | (0x000000FF & buffer[0]);
  
  recordContents.append("{").append("firstCDRIndex").append("=").append(firstCDRIndex).append("};");
      
      
       int seqIntCDR = 0;
       buffer = asnpr.readBlock(1);
       seqIntCDR |= (0x000000FF & buffer[0]);
       
   recordContents.append("{").append("sequenceOfIntermediateCDR").append("=").append(seqIntCDR).append("};");
   
       int recordType = 0;
       int userType = 0;
       buffer = asnpr.readBlock(1);
       
       recordType |= (0x000000FF & (buffer[0] & 0xE0));
       
       userType |= (0x000000FF & (buffer[0] & 0x1F));
   recordContents.append("{").append("sequence of intermediate CDR").append("=").append(seqIntCDR).append("usertype").append("=").append(userType).append("};");
       
       int causeterm = 0;
       buffer = asnpr.readBlock(1);
       
       causeterm  |= (0x000000FF & buffer[0]);
   recordContents.append("{").append("causeForTermination").append("=").append(causeterm).append("};");
        
        
        int causepartterm = 0;
       buffer = asnpr.readBlock(1);
       
       causepartterm  |= (0x000000FF & buffer[0]);
        
     recordContents.append("{").append("causeForPartialRecordTermination").append("=").append(causepartterm).append("};");
      
        
         buffer = asnpr.readBlock(8);
       
       String localmscin = asnpr.parseBCDString(buffer);
      
    recordContents.append("{").append("servedIMSI").append("=").append(localmscin).append("};");
        
        buffer = asnpr.readBlock(8);
        
        
        buffer = asnpr.readBlock(2);
       String initialLAC = asnpr.parseInteger(buffer);
       recordContents.append("{").append("current/initial LAC").append("=").append(initialLAC).append("};");
    
       
       buffer = asnpr.readBlock(18);
       
        buffer = asnpr.readBlock(1);
       
        int Freeind = 0;
        int notuse = 0;
        notuse |= (0x000000FF & (buffer[0] & 0x00));
        Freeind |= (0x000000FF & (buffer[0] & 0x0F));
   recordContents.append("{").append("notInUse").append("=").append(notuse).append("freeIndication").append("=").append(Freeind).append("};");
        
       
       byte[] cdr = asnpr.readBlock(1);
       
       int rf = 0;
       int rtt = 0;
       int iwfserv = 0;
          int  iwfcode =0;
       rtt |= (0x000000FF & (cdr[0] & 0x80));
       iwfserv |= (0x000000FF & (cdr[0] & 0x40));
       iwfcode |= (0x000000FF & (cdr[0] & 0x3F));    
       
  recordContents.append("{").append("Roaming_flag").append("=").append(rtt).append("tele_iwf_service").append("=").append(iwfserv).append("tele_iwf_code").append("=").append(iwfcode).append("};");
      
       //check
       
       
       buffer = asnpr.readBlock(15);
       
        buffer = asnpr.readBlock(4);
        int nobytes = 0;
        nobytes  |= ((0x000000FF & buffer[3]) << 24)
                  | (0x000000FF & buffer[1] << 16)
                   | (0x000000FF & buffer[2] << 8)
              | (0x000000FF & buffer[0]);
       
     recordContents.append("{").append("numberOfBytes").append("=").append(nobytes).append("};");
      
       
       
       buffer = asnpr.readBlock(8);
       String SMCId = asnpr.parseBCDString(buffer);
   recordContents.append("{").append("SMC_ID").append("=").append(SMCId).append("};");
         
        buffer = asnpr.readBlock(3);
       String initialMSC = asnpr.parseInteger(buffer);
      
  recordContents.append("{").append("caller-initialMSC").append("=").append(initialMSC).append("};");
        
        buffer = asnpr.readBlock(3);
       String currentMSC = asnpr.parseInteger(buffer);
  
  recordContents.append("{").append("caller-currentMSC").append("=").append(currentMSC).append("};");
       
        buffer = asnpr.readBlock(3);
       String cinitialMSC = asnpr.parseInteger(buffer);
      
   recordContents.append("{").append("called-initialMSC").append("=").append(cinitialMSC).append("};");
      
       
        buffer = asnpr.readBlock(3);
       String ccurrentMSC = asnpr.parseInteger(buffer);
      
  recordContents.append("{").append("called-currentMSC").append("=").append(ccurrentMSC).append("};");
        
       buffer = asnpr.readBlock(2);
       
       int usergroup = 0;
       
      usergroup  |= (0x000000FF & buffer[1] << 8)
              | (0x000000FF & buffer[0]);
       
   recordContents.append("{").append("charged_user_group_number").append("=").append(usergroup).append("};");
      
       
       buffer = asnpr.readBlock(1);
       String chargedflag = asnpr.parseBytes(buffer);
     
  recordContents.append("{").append("call_in_group_charged_flag").append("=").append(chargedflag).append("};");
        
       
       buffer = asnpr.readBlock(1);
       
       String PSTNflag = asnpr.parseInteger(buffer);
       
   recordContents.append("{").append("causeForTermination").append("=").append(PSTNflag).append("};");
       
        buffer = asnpr.readBlock(4);
        int pulse = 0;
        pulse  |= ((0x000000FF & buffer[3]) << 24)
                  | (0x000000FF & buffer[1] << 16)
                   | (0x000000FF & buffer[2] << 8)
              | (0x000000FF & buffer[0]);
 
   recordContents.append("{").append("chargePulseNumber").append("=").append(pulse).append("};");
       
       
   buffer = asnpr.readBlock(1);
       
       String meter = asnpr.parseInteger(buffer);
     
   recordContents.append("{").append("meter_indication value").append("=").append(meter).append("};");
        
        buffer = asnpr.readBlock(1);
       
       String charge = asnpr.parseInteger(buffer);
  
   recordContents.append("{").append("charge_method").append("=").append(charge).append("};");
        
        
        buffer = asnpr.readBlock(1);
       
        int tempcredit = 0;
        int bypass = 0;
        int hotbilling = 0;
        int userzoneidtype = 0;
        int nouse =0;
        tempcredit |= (0x000000FF & (buffer[0] & 0x01));
        bypass |= (0x000000FF & (buffer[0] & 0x06));
        hotbilling |= (0x000000FF & (buffer[0] & 0x08));
        bypass |= (0x000000FF & (buffer[0] & 0x10));
        nouse |= (0x000000FF & (buffer[0] & 0xE0));
    
  recordContents.append("{").append("bypass").append("=").append(bypass).append("};");
   recordContents.append("{").append("hotbilling").append("=").append(hotbilling).append("};");
   recordContents.append("{").append("charge_method").append("=").append(bypass).append("};");
   recordContents.append("{").append("charge_method").append("=").append(nouse).append("};");
        
       
        int callconnection = 0;
       buffer = asnpr.readBlock(1);
       
       callconnection  |= (0x000000FF & buffer[0]);
 
   recordContents.append("{").append("call connection duration is").append("=").append(callconnection).append("};");
        
        
        buffer = asnpr.readBlock(18);
       byte[] newBuf1 = new byte[16];
       int j = 2;
       for(j = 2; j < 18; j++) {
           
           newBuf[j-2] = buffer[j];
          
      
        }
       
       String outpulsed = asnpr.parseBCDString(newBuf1);
       
    
  recordContents.append("{").append("outpulsedValue ").append("=").append(outpulsed).append("};");
  
       
        buffer = asnpr.readBlock(8);
       
       String callref = asnpr.parseBytes(buffer);
        
    recordContents.append("{").append("callReferenceNumber").append("=").append(callref).append("};");
       
         buffer = asnpr.readBlock(6);
         
         buffer = asnpr.readBlock(2);
       
       String userzoneid = asnpr.parseBytes(buffer);
      
        
     recordContents.append("{").append("userZzoneId").append("=").append(userzoneid).append("};");
       

       
    FlatRecord tmpRecord = new FlatRecord(recordContents.toString());
    
    return tmpRecord;
  }  
  
 /**
  * process the sub record of the CDR. This could be recursive, but as the
  * structure is more or less flat, we are doing this as a subroutine, just to
  * keep it clear.
  * 
  * @param parentTag the tag of the parent record
  * @param subRecord the sub record to process
  * @throws Exception 
  */
  private String processSubRecord(String parentTag, byte[] subRecord) throws Exception
  {
    Asn1Class output;
    StringBuilder recordContents = new StringBuilder();
    ASN1Parser asnps = new ASN1Parser(asn1Specification);
  
    asnps.setDataToParse(subRecord);
    
    // Split up the CDRs
    while (asnps.ready())
    {
      // get the cdr header
      output = asnps.readNextElement();
      
      // Output the information if we can
      String tagIndex = parentTag + ";" + output.getRawTag();
      if (asn1Specification.getTagType(tagIndex) >= 0)
      {
        //System.out.println("Mapped CDR field (" + tagIndex + "=" + asn1Specification.getTagName(tagIndex) + "), Length: " + output.getLength() + ", Value: " + asnps.parseASN1(asn1Specification.getTagType(tagIndex), output.getOrigValue()));
        recordContents.append("{").append(asn1Specification.getTagName(tagIndex)).append("=").append(asnps.parseASN1(asn1Specification.getTagType(tagIndex), output.getOrigValue())).append("};");
      }
      else
      {
        //System.out.println("-----> CDR field (" + tagIndex + ") [" +output.getTag() + "], Length: " + output.getLength() + ", Value: " + asnps.parseBytes(output.getOrigValue()));
      }
    }
    
    return recordContents.toString();
  }
    
}
