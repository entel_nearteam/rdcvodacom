/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Limited 2006-2012.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of architecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are discplaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
package EnterTeleProbe.Decoding;

import OpenRate.adapter.file.BinaryFileInputAdapter;
import OpenRate.record.IRecord;
import OpenRate.record.RatingRecord;
import java.util.ArrayList;
import OpenRate.parser.ASN1Parser;
import OpenRate.parser.Asn1Class;
import OpenRate.exception.ProcessingException;
import OpenRate.record.FlatRecord;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Binary input file adapter. As the files are not that large, we attempt to
 * parse them all in one go. This means that we load the entire binary file
 * contents into memory and then run through it decoding. This gives us a huge
 * performance boost over random access reading.
 */
public class HuaweiASN1InputAdapter extends BinaryFileInputAdapter
{
  // this is the CVS version info
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: BalInputAdapter.java,v $, $Revision: 1.1 $, $Date: 2012/01/29 13:31:51 $";

  // This is the object that is used to compress the records
  RatingRecord tmpDataRecord = null;

  // Defintion of the tags and so on
  HuaweiDef asn1Specification = new HuaweiDef();
  
  // our ASN1 parser object
  ASN1Parser asnp;
  
  // -----------------------------------------------------------------------------
  // ------------------ Start of inherited Plug In functions ---------------------
  // -----------------------------------------------------------------------------

 /**
  * This is called when the synthetic Header record is encountered, and has the
  * meaning that the stream is starting. In this example we have nothing to do
  * 
  * @param r The record we are working on
  * @return The processed record
  */
  @Override
  public IRecord procHeader(IRecord r)
  {
    asn1Specification.initTags();
    asnp = new ASN1Parser(asn1Specification);
  
    return r;
  }

 /**
  * This is called when a data record is encountered. You should do any normal
  * processing here. For the input adapter, we probably want to change the 
  * record type from FlatRecord to the record(s) type that we will be using in
  * the processing pipeline.
  *
  * This is also the location for accumulating records into logical groups
  * (that is records with sub records) and placing them in the pipeline as
  * they are completed. If you receive a sub record, simply return a null record
  * in this method to indicate that you are handling it, and that it will be
  * purged at a later date.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procValidRecord(IRecord r)
  {
    return r;
  }

 /**
  * This is called when a data record with errors is encountered. You should do
  * any processing here that you have to do for error records, e.g. statistics,
  * special handling, even error correction!
  * 
  * The input adapter is not expected to provide any records here.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procErrorRecord(IRecord r)
  {
    return r;
  }

 /**
  * This is called when the synthetic trailer record is encountered, and has the
  * meaning that the stream is now finished. In this example, all we do is 
  * pass the control back to the transactional layer.
  *
  * In models where record aggregation (records and sub records) is used, you
  * might want to check for any purged records here.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procTrailer(IRecord r)
  {
    return r;
  }

 /**
  * This is where the binary parsing happens.
  * 
  * @param fileContents The contents of the binary file
  * @return An array of records to push into the pipe
  */
  @Override
  public ArrayList<IRecord> parseBinaryFileContents(byte[] fileContents) {
    ArrayList<IRecord> outBatch = new ArrayList<>();
    
    // Current asn1 element we are working on
    Asn1Class output;

    // Set up the data to parse
    asnp.setDataToParse(fileContents);
    
    try {
      // Header
      output = asnp.readNextElement();
      getPipeLog().info("File length <" + output.getLength() + ">");
      //System.out.println("Header (" + output.getRawTag() + ") File length: " + output.getLength());

      // read the header block
      output = asnp.readNextElement();
      //System.out.println("Read header block (" + output.getLength() + ")");
      byte[] headerBlock = asnp.readBlock(output.getLength());      
      
      // read the call data block
      output = asnp.readNextElement();
      //System.out.println("Read call detail block (" + output.getLength() + ")");
      byte[] cdrBlock = asnp.readBlock(output.getLength());      

      // trailer header
      output = asnp.readNextElement();
      //System.out.println("Read Trailer block (" + output.getLength() + ")");      
      byte[] trailerBlock = asnp.readBlock(output.getLength());      
      
      // get the end tag - just bung it away
      asnp.readNextElement();
      
      // Check that we have finished the file correctly
      if (asnp.ready())
      {
        throw new ProcessingException("ASN1 File not ended correctly.",getSymbolicName());
      }
      
      // finished reading the file, now process the CDRs
      processHeaderBlock(headerBlock);
      
      // finished reading the file, now process the CDRs
      outBatch.addAll(processCDRBlock(cdrBlock));
      
      // finished reading the file, now process the CDRs
      processTrailerBlock(trailerBlock);
    } catch (Exception ex) {
      Logger.getLogger(HuaweiASN1InputAdapter.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    return outBatch;
  }
  
 /**
  * process the Header block.
  * 
  * @param cdrBlock the CDR block to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private int processHeaderBlock(byte[] headerBlock) throws Exception
  {
    Asn1Class output;
    
    return 0;
  }
  
 /**
  * process the Trailer block.
  * 
  * @param cdrBlock the CDR block to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private int processTrailerBlock(byte[] trailerBlock) throws Exception
  {
    Asn1Class output;
    
    return 0;
  }
  
 /**
  * process the CDR block - this is the collection of all the CDRs in a file.
  * This method unpacks the CDR list into individual CDRs so they can be
  * processed.
  * 
  * @param cdrBlock the CDR block to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private ArrayList<IRecord> processCDRBlock(byte[] cdrBlock) throws Exception
  {
    Asn1Class output;
    ArrayList<IRecord> outBatch = new ArrayList<>();
    
    int cdrCounter = 1;
            
    // Set up the data
    asnp.setDataToParse(cdrBlock);

    // Split up the CDRs
    while (asnp.ready())
    {
      // get the cdr header
      output = asnp.readNextElement();
      //System.out.println("Read call detail record (" + output.getLength() + ")");
      
      // get the CDR to process
      byte[] cdrRecord = asnp.readBlock(output.getLength());      

      // process the CDR
      IRecord record = processCDR(cdrRecord);

      // Number it
      record.setRecordID(cdrCounter++);
      
      // Add the record to the output batch
      outBatch.add(record);
    }
    
    // how many we processed
    return outBatch;
  }

 /**
  * process the CDR.
  * 
  * @param cdrRecord the CDR to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private IRecord processCDR(byte[] cdrRecord) throws Exception
  {
    Asn1Class output;
    StringBuilder recordContents = new StringBuilder();
    ASN1Parser asnpr = new ASN1Parser(asn1Specification);
    
    // Set up the data for the record
    asnpr.setDataToParse(cdrRecord);
    
    // get the cdr type
    output = asnpr.readNextElement();
    //System.out.println("CDR type (" + output.getRawTagHex() + "), Length: " + output.getLength() + ", type: " + asnp.parseInteger(output.getOrigValue()));    

    // The CDR Typw is used in controlling the filtering and naming, as well as
    // the type interpretation. Because Huawei does not stick to a 1 tag, 1
    // meaning policy, each cdr must be interpreted differently, which sucks
    String cdrType = asnp.parseBytes(output.getOrigValue());
    //System.out.println("CDR type " + asn1Specification.getCDRName(cdrType) + " (" + cdrType + ")");  
    recordContents.append(asn1Specification.getCDRName(cdrType)).append(";");
    
    while (asnpr.ready())
    {
      // get the cdr header
      output = asnpr.readNextElement();
      //System.out.println("Read CDR field (" + output.getRawTagHex() + "), Length: " + output.getLength());
      
      String tagIndex = cdrType + ";" + output.getRawTag();
      
      // Calculate the tag type for the lookup out of CDR type and cdr field tag
      if (output.isConstructed())
      {
        // process this sub block
        byte[] subRecord = asnpr.readBlock(output.getLength());
        recordContents.append(processSubRecord(tagIndex,subRecord));
      }
      
      // Output the information if we can
      if (asn1Specification.getTagType(tagIndex) >= 0)
      {
        //System.out.println("Mapped CDR field (" + tagIndex + "=" + asn1Specification.getTagName(tagIndex) + "), Length: " + output.getLength() + ", Value: " + asnpr.parseASN1(asn1Specification.getTagType(tagIndex), output.getOrigValue()));
        recordContents.append("{").append(asn1Specification.getTagName(tagIndex)).append("=").append(asnpr.parseASN1(asn1Specification.getTagType(tagIndex), output.getOrigValue())).append("};");
      }
      else
      {
        //System.out.println("-----> CDR field (" + tagIndex + ") [" +output.getTag() + "], Length: " + output.getLength() + ", Value: " + asnpr.parseBytes(output.getOrigValue()));
      }
    }

    FlatRecord tmpRecord = new FlatRecord(recordContents.toString());
    
    return tmpRecord;
  }  
  
 /**
  * process the sub record of the CDR. This could be recursive, but as the
  * structure is more or less flat, we are doing this as a subroutine, just to
  * keep it clear.
  * 
  * @param parentTag the tag of the parent record
  * @param subRecord the sub record to process
  * @throws Exception 
  */
  private String processSubRecord(String parentTag, byte[] subRecord) throws Exception
  {
    Asn1Class output;
    StringBuilder recordContents = new StringBuilder();
    ASN1Parser asnps = new ASN1Parser(asn1Specification);
  
    asnps.setDataToParse(subRecord);
    
    // Split up the CDRs
    while (asnps.ready())
    {
      // get the cdr header
      output = asnps.readNextElement();
      
      // Output the information if we can
      String tagIndex = parentTag + ";" + output.getRawTag();
      if (asn1Specification.getTagType(tagIndex) >= 0)
      {
        //System.out.println("Mapped CDR field (" + tagIndex + "=" + asn1Specification.getTagName(tagIndex) + "), Length: " + output.getLength() + ", Value: " + asnps.parseASN1(asn1Specification.getTagType(tagIndex), output.getOrigValue()));
        recordContents.append("{").append(asn1Specification.getTagName(tagIndex)).append("=").append(asnps.parseASN1(asn1Specification.getTagType(tagIndex), output.getOrigValue())).append("};");
      }
      else
      {
        //System.out.println("-----> CDR field (" + tagIndex + ") [" +output.getTag() + "], Length: " + output.getLength() + ", Value: " + asnps.parseBytes(output.getOrigValue()));
      }
    }
    
    return recordContents.toString();
  }
}
