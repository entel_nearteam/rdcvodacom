/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EnterTeleProbe.Decoding;
import OpenRate.parser.ASN1Parser;
import OpenRate.parser.IASN1Def;
import java.util.HashMap;

/**
 *
 * @author hmarikan
 */
public class AlcatelSSPDef implements IASN1Def {
    
      // These control the decoding and naming of the fields
  private HashMap<String,String> tagNames;
  private HashMap<String,Integer> tagTypes;
  private HashMap<String,String> cdrNames;
  
  @Override
  public void initASN1() {
  }

 /**
  * Initialise the tags for the decoding. The key into the hash maps is 
  * CDRtype hex;Tag ID hex[;Block ID Hex]
  */
  @Override
  public void initTags() {
    // Set up the hash maps
    tagNames = new HashMap<>();
    tagTypes = new HashMap<>();
    cdrNames = new HashMap<>();
    
    // MOC
   cdrNames.put("00", "MOC");
    tagNames.put("00;81", "IMSI");
    tagTypes.put("00;81", ASN1Parser.BCDString);
    tagNames.put("00;82", "IMEI");
    tagTypes.put("00;82", ASN1Parser.BCDString);
    tagNames.put("00;83", "MSISDN");
    tagTypes.put("00;83", ASN1Parser.BCDString);
    tagNames.put("00;84", "CallingNumber");
    tagTypes.put("00;84", ASN1Parser.BCDString);
    tagNames.put("00;85", "CalledNumber");
    tagTypes.put("00;85", ASN1Parser.BCDString);
    tagNames.put("00;97", "AnswerTimestamp");
    tagTypes.put("00;97", ASN1Parser.BCDString);
    tagNames.put("00;98", "ReleaseTimestamp");
    tagTypes.put("00;98", ASN1Parser.BCDString);
    tagNames.put("00;99", "Duration");
    tagTypes.put("00;99", ASN1Parser.INTEGER); 
    
    /* //MO
     cdrNames.put("00", "MO");
    tagNames.put("00;81", "servedIMSI");
    tagTypes.put("00;81", ASN1Parser.BCDStringLE);
    tagNames.put("00;82", "servedIMEI");
    tagTypes.put("00;82", ASN1Parser.BCDStringLE);
    tagNames.put("00;84",  "callingNumber");
    tagTypes.put("00;84", ASN1Parser.BCDStringLE);
    tagNames.put("00;85", "calledNumber");
    tagTypes.put("00;85", ASN1Parser.BCDStringLE);
    tagNames.put("00;86", "translatedNumber");
    tagTypes.put("00;86", ASN1Parser.BCDStringLE);
    tagNames.put("00;99",  "callDuration");
    tagTypes.put("00;99",  ASN1Parser.INTEGER);
    tagNames.put("00:aa", "mscIncomingROUTE");
    tagTypes.put("00:aa", ASN1Parser.ENUMERATED);
    tagNames.put("00:ab", "mscOutgoingROUTE");
    tagTypes.put("00:ab", ASN1Parser.ENUMERATED);
    tagNames.put("00;ac", "location");
    tagTypes.put("00;ac", ASN1Parser.SEQUENCE); 
    tagNames.put("00:9f814d", "callType");
    tagTypes.put("00:9f814d", ASN1Parser.ENUMERATED);
    tagNames.put("00;9f8168",  "recordNumber");
    tagTypes.put("00;9f8168",  ASN1Parser.INTEGER);
    tagNames.put("00;9f8233",  "ringingDuration");
    tagTypes.put("00;9f8233",  ASN1Parser.INTEGER);
 
    
   
   
   
   
  
   
   
    
    // MOC
    /*cdrNames.put("01", "MTC");
    tagNames.put("01;81", "IMSI");
    tagTypes.put("01;81", ASN1Parser.BCDString);
    tagNames.put("01;82", "IMEI");
    tagTypes.put("01;82", ASN1Parser.BCDString);
    tagNames.put("01;83", "MSISDN");
    tagTypes.put("01;83", ASN1Parser.BCDString);
    tagNames.put("01;84", "CallingNumber");
    tagTypes.put("01;84", ASN1Parser.BCDString);
    tagNames.put("01;94", "AnswerTimestamp");
    tagTypes.put("01;94", ASN1Parser.BCDString);
    tagNames.put("01;95", "ReleaseTimestamp");
    tagTypes.put("01;95", ASN1Parser.BCDString);
    tagNames.put("01;96", "Duration");
    tagTypes.put("01;96", ASN1Parser.INTEGER);
    
    // Transit calls
    cdrNames.put("04", "TRANSIT");
    
    // SMMO
    cdrNames.put("07", "SMMO");
    tagNames.put("06;81", "IMSI");
    tagTypes.put("06;81", ASN1Parser.BCDString);
    tagNames.put("06;82", "IMEI");
    tagTypes.put("06;82", ASN1Parser.BCDString);
    tagNames.put("06;83", "MSISDN");
    tagTypes.put("06;83", ASN1Parser.BCDString);
    tagNames.put("06;89", "Timestamp");
    tagTypes.put("06;89", ASN1Parser.BCDString);
    tagNames.put("06;8c", "CalledNumber");
    tagTypes.put("06;8c", ASN1Parser.BCDString);
    
    // SMMT
    cdrNames.put("06", "SMMT");
    tagNames.put("07;82", "IMSI");
    tagTypes.put("07;82", ASN1Parser.BCDString);
    tagNames.put("07;83", "IMEI");
    tagTypes.put("07;83", ASN1Parser.BCDString);
    tagNames.put("07;84", "MSISDN");
    tagTypes.put("07;84", ASN1Parser.BCDString);
    tagNames.put("07;88", "Timestamp");
    tagTypes.put("07;88", ASN1Parser.BCDString);
    
    // SUPS
    cdrNames.put("0a", "SUPS"); */
  }

  @Override
  public String getTagName(String tagPath)
  {
    if (tagNames.containsKey(tagPath))
    {
      return tagNames.get(tagPath);
    }
    else
    {
      return "";
    }
  }
  
  public String getCDRName(String tagPath)
  {
    if (cdrNames.containsKey(tagPath))
    {
      return cdrNames.get(tagPath);
    }
    else
    {
      return "Unknown (" + tagPath + ")";
    }
  }
  
  @Override
  public int getTagType(String tagPath)
  {
    if (tagTypes.containsKey(tagPath))
    {
      return tagTypes.get(tagPath);
    }
    else
    {
      return -1;
    }
  }
    
}
