/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EnterTeleProbe.Decoding;
import OpenRate.adapter.file.BinaryFileInputAdapter;
import OpenRate.record.IRecord;
import OpenRate.record.RatingRecord;
import java.util.ArrayList;
import OpenRate.parser.ASN1Parser;
import OpenRate.parser.Asn1Class;
import OpenRate.record.FlatRecord;
import java.util.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hmarikan
 */
public class Alcatel3GPPNGNInputAdapter extends BinaryFileInputAdapter {
    
  // this is the CVS version info
  public static String CVS_MODULE_INFO = "OpenRate, $RCSfile: BalInputAdapter.java,v $, $Revision: 1.1 $, $Date: 2012/01/29 13:31:51 $";

  // This is the object that is used to compress the records
  RatingRecord tmpDataRecord = null;

  // Defintion of the tags and so on
  Alcatel3GPPNGNDef asn1Specification = new Alcatel3GPPNGNDef();
  
  // our ASN1 parser object
  ASN1Parser asnp;
  
  // -----------------------------------------------------------------------------
  // ------------------ Start of inherited Plug In functions ---------------------
  // -----------------------------------------------------------------------------

 /**
  * This is called when the synthetic Header record is encountered, and has the
  * meaning that the stream is starting. In this example we have nothing to do
  * 
  * @param r The record we are working on
  * @return The processed record
  */
  @Override
  public IRecord procHeader(IRecord r)
  {
    asn1Specification.initTags();
    asnp = new ASN1Parser(asn1Specification);
  
    return r;
  }

 /**
  * This is called when a data record is encountered. You should do any normal
  * processing here. For the input adapter, we probably want to change the 
  * record type from FlatRecord to the record(s) type that we will be using in
  * the processing pipeline.
  *
  * This is also the location for accumulating records into logical groups
  * (that is records with sub records) and placing them in the pipeline as
  * they are completed. If you receive a sub record, simply return a null record
  * in this method to indicate that you are handling it, and that it will be
  * purged at a later date.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procValidRecord(IRecord r)
  {
    return r;
  }

 /**
  * This is called when a data record with errors is encountered. You should do
  * any processing here that you have to do for error records, e.g. statistics,
  * special handling, even error correction!
  * 
  * The input adapter is not expected to provide any records here.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procErrorRecord(IRecord r)
  {
    return r;
  }

 /**
  * This is called when the synthetic trailer record is encountered, and has the
  * meaning that the stream is now finished. In this example, all we do is 
  * pass the control back to the transactional layer.
  *
  * In models where record aggregation (records and sub records) is used, you
  * might want to check for any purged records here.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procTrailer(IRecord r)
  {
    return r;
  }

 /**
  * This is where the binary parsing happens.
  * 
  * @param fileContents The contents of the binary file
  * @return An array of records to push into the pipe
  */
  @Override
  public ArrayList<IRecord> parseBinaryFileContents(byte[] fileContents) {
    ArrayList<IRecord> outBatch = new ArrayList<>();
    
    try {
      // No header - nothing to do for header

      // now process the CDRs block - this is the only element in the file!
      outBatch.addAll(processCDRBlock(fileContents));

      // No Trailer
    } catch (Exception ex) {
      Logger.getLogger(AXEASN1InputAdapter.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    return outBatch;
  }
  
 /**
  * process the CDR block - this is the collection of all the CDRs in a file.
  * This method unpacks the CDR list into individual CDRs so they can be
  * processed.
  * 
  * @param cdrBlock the CDR block to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private ArrayList<IRecord> processCDRBlock(byte[] cdrBlock) throws Exception
  {
    Asn1Class output;
    ArrayList<IRecord> outBatch = new ArrayList<>();
    
    int cdrCounter = 1;
    
    // Set up the data to parse
    asnp.setDataToParse(cdrBlock);

    // Split up the CDRs
    while (asnp.ready())
    {
      // CDRs are presented as a straight list of CDRs
      try {
        output = asnp.readNextElement();
        
        // AXE uses block padding, and we have to skip over this
        if (output.isNullTag())
        {
          //System.out.print(".");
        }
        else if (output.getLength() == 0)
        {
          // just skip over it, it is a container
        }
        else
        {
          //System.out.println("-----> CDR field (" + tagIndex + ") [" +output.getTag() + "], Length: " + output.getLength() + ", Value: " + asnp.parseBytes(output.getOrigValue()));

        	int cdrType = output.getTag();
        	// get the CDR to process
          byte[] cdrRecord = asnp.readBlock(output.getLength());      

          // process the CDR
          if (cdrType == 0 || cdrType == 1) 
          {
        	  IRecord record = processCDR(cdrRecord);
        	  
        	  if (record != null) {
        		  // Number it
        		  record.setRecordID(cdrCounter++);
        		  
        		  // Add the record to the output batch
        		  outBatch.add(record);
        		  
        	  }
			
		}
        }
      } catch (Exception ex) {
        Logger.getLogger(AXEASN1InputAdapter.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    
    return outBatch;
  }
  
 /**
  * process the CDR.
  * 
  * @param cdrRecord the CDR to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private IRecord processCDR(byte[] cdrRecord) throws Exception
  {
    Asn1Class output;
    StringBuilder recordContents = new StringBuilder();
    ASN1Parser asnpr = new ASN1Parser(asn1Specification);
    
    // Set up the data for the record
    asnpr.setDataToParse(cdrRecord);
    
    // get the cdr type
    output = asnpr.readNextElement();
//    System.out.println("CDR type " + output.getTag() + " (" + output.getRawTag() + "), Length: " + output.getLength() + ", type: " + asnp.parseInteger(output.getOrigValue()));

    // The CDR Type is used in controlling the filtering and naming, as well as
    // the type interpretation. Because Huawei does not stick to a 1 tag, 1
    // meaning policy, each cdr must be interpreted differently, which sucks
//    String cdrType = Integer.toString(output.getTag());
    String cdrType = asnpr.parseInteger((output.getOrigValue()));
//    System.out.println("CDR type " + asn1Specification.getCDRName(cdrType) + " (" + cdrType + ")"); 
    recordContents.append(asn1Specification.getCDRName(cdrType)).append(";");
    
    while (asnpr.ready())
    {
      // get the cdr header
      output = asnpr.readNextElement();
      //System.out.println("Read CDR field (" + output.getRawTagHex() + "), Length: " + output.getLength());
      
      String tagIndex = cdrType + ";" + output.getRawTag();
      // Calculate the tag type for the lookup out of CDR type and cdr field tag
//      System.out.println("UNCONSTRUCTED CDR field (" + newTag + ") [" +output.getRawTag() + "], Length: " + output.getLength() + ", Value: " + asnpr.parseBytes(output.getOrigValue()));
      if (output.isConstructed())
      {
        // process this sub block
        byte[] subRecord = asnpr.readBlock(output.getLength());
        recordContents.append(processSubRecord(tagIndex,subRecord));
      }
      
      // Output the information if we can
      if (asn1Specification.getTagType(tagIndex) >= 0)
      {
        // deal with custom decodings
        if (asn1Specification.getTagType(tagIndex) >= 0xF0)
        {
          // we need to do a custom decoding
          //System.out.println("Mapped CDR field (" + tagIndex + "=" + asn1Specification.getTagName(tagIndex) + "), Length: " + output.getLength() + ", Value: " + decodeEricsson(asn1Specification.getTagType(tagIndex), output.getOrigValue()));
          recordContents.append("{").append(asn1Specification.getTagName(tagIndex)).append("=").append(decodeEricsson(asn1Specification.getTagType(tagIndex), output.getOrigValue())).append("};");
        }
        else
        {
          // we can use the standard decoding
          //System.out.println("Mapped CDR field (" + tagIndex + "=" + asn1Specification.getTagName(tagIndex) + "), Length: " + output.getLength() + ", Value: " + asnpr.parseASN1(asn1Specification.getTagType(tagIndex), output.getOrigValue()));
          recordContents.append("{").append(asn1Specification.getTagName(tagIndex)).append("=").append(asnpr.parseASN1(asn1Specification.getTagType(tagIndex), output.getOrigValue())).append("};");
        }
      }
      else
      {
//        System.out.println("-----> CDR field (" + tagIndex + ") [" +output.getTag() + "], Length: " + output.getLength() + ", Value: " + asnpr.parseBytes(output.getOrigValue()));
      }
    }

    FlatRecord tmpRecord = new FlatRecord(recordContents.toString());
    
    return tmpRecord;
  }  
  
 /**
  * process the sub record of the CDR. This could be recursive, but as the
  * structure is more or less flat, we are doing this as a subroutine, just to
  * keep it clear.
  * 
  * @param parentTag the tag of the parent record
  * @param subRecord the sub record to process
  * @throws Exception 
  */
  private String processSubRecord(String parentTag, byte[] subRecord) throws Exception
  {
    Asn1Class output;
    StringBuilder recordContents = new StringBuilder();
    ASN1Parser asnps = new ASN1Parser(asn1Specification);
  
    asnps.setDataToParse(subRecord);
    
    // Split up the CDRs
    while (asnps.ready())
    {
      // get the cdr header
      output = asnps.readNextElement();
      
      // Output the information if we can
      String tagIndex = parentTag + ";" + output.getRawTag();
      
      if (asn1Specification.getTagType(tagIndex) >= 0)
      {
        // deal with custom decodings
        if (asn1Specification.getTagType(tagIndex) >= 0xF0)
        {
          // we need to do a custom decoding
          //System.out.println("Mapped CDR field (" + tagIndex + "=" + asn1Specification.getTagName(tagIndex) + "), Length: " + output.getLength() + ", Value: " + decodeEricsson(asn1Specification.getTagType(tagIndex), output.getOrigValue()));
          recordContents.append("{").append(asn1Specification.getTagName(tagIndex)).append("=").append(decodeEricsson(asn1Specification.getTagType(tagIndex), output.getOrigValue())).append("};");
        }
        else
        {
          // we can use the standard decoding
          //System.out.println("Mapped CDR field (" + tagIndex + "=" + asn1Specification.getTagName(tagIndex) + "), Length: " + output.getLength() + ", Value: " + asnps.parseASN1(asn1Specification.getTagType(tagIndex), output.getOrigValue()));
          recordContents.append("{").append(asn1Specification.getTagName(tagIndex)).append("=").append(asnps.parseASN1(asn1Specification.getTagType(tagIndex), output.getOrigValue())).append("};");
        }
      }
      else
      {
//        System.out.println("CONSTRUCTED CDR field (" + tagIndex + ") [" +output.getTag() + "], Length: " + output.getLength() + ", Value: " + asnps.parseBytes(output.getOrigValue()));
      }
    }
    
    return recordContents.toString();
  }
  
  /**
   * Parse the ASN.1 according to funky Ericsson ways
   *
   * @param tag The tag
   * @param value The value byte array
   * @return The string
   * @throws ASN1Exception
   */
  public String decodeEricsson(int tagType, byte[] value)
  {
    switch(tagType)
    {
      case 0xF1: // Ericsson date (yymmdd)
      {
        if (value.length >= 6)
        {
          String callTime = asnp.parseBCDString(value);
          return callTime.substring(0, 12);
        }
        else
        {
          return "Unknown E3 date format";
        }
      }
        
      case 0xF2: // Ericsson time (in hhmmss)
      {
        if (value.length == 3)
        {
          StringBuilder buf = new StringBuilder(value.length * 2);

          Formatter formatter = new Formatter(buf);  
          for (byte b : value) {  
              formatter.format("%02d", b);  
          }
          return buf.toString();
        }
        else
        {
          return "Unknown E3 time format";
        }
      }
        
      case 0xF3: // Ericsson duration (in hhmmss)
      {
        if (value.length == 3)
        {
          int dur = value[0] * 3600 + value[1] * 60 + value[2];
          return Integer.toString(dur);
        }
        else
        {
          return "Unknown E3 time format";
        }
      }
      case 0xF4: // LAC + cellId
      {
    	  if (value.length == 2)
    	  {
    		  
    		  int cellId = ((value[0] & 0xFF) << 8) + (value[1] & 0xFF);
    		  return Integer.toString(cellId);
    	  }
    	  else
    	  {
    		  return "Unknown E3 time format";
    	  }
      }
    }
    return "";
  }
}

    

