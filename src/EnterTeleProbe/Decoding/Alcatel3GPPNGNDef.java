/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EnterTeleProbe.Decoding;
import OpenRate.parser.ASN1Parser;
import OpenRate.parser.IASN1Def;
import java.util.HashMap;


/**
 *
 * @author hmarikan
 */
public class Alcatel3GPPNGNDef implements IASN1Def {
    
  // These control the decoding and naming of the fields
  private HashMap<String,String> tagNames;
  private HashMap<String,Integer> tagTypes;
  private HashMap<String,String> cdrNames;
  
  @Override
  public void initASN1() {
  }

 /**
  * Initialise the tags for the decoding. The key into the hash maps is 
  * CDRtype hex;Tag ID hex[;Block ID Hex]
  */
  @Override
  public void initTags() {
    // Set up the hash maps
    tagNames = new HashMap<>();
    tagTypes = new HashMap<>();
    cdrNames = new HashMap<>();
    
    // MO CDR
    cdrNames.put("0", "MO");
    tagNames.put("0;81", "servedIMSI");
    tagTypes.put("0;81", ASN1Parser.BCDStringLE);
    tagNames.put("0;82", "servedIMEI");
    tagTypes.put("0;82", ASN1Parser.BCDStringLE);
    tagNames.put("0;84",  "callingNumber");
    tagTypes.put("0;84", ASN1Parser.BCDStringLE);
    tagNames.put("0;85", "calledNumber");
    tagTypes.put("0;85", ASN1Parser.BCDStringLE);
    tagNames.put("0;86", "translatedNumber");
    tagTypes.put("0;86", ASN1Parser.BCDStringLE);
    tagNames.put("0;96",  "seizureTime");
    tagTypes.put("0;96",  ASN1Parser.BCDStringLE);
    tagNames.put("0;99",  "callDuration");
    tagTypes.put("0;99",  ASN1Parser.INTEGER);
    tagNames.put("0:aa", "mscIncomingROUTE");
    tagTypes.put("0:aa", ASN1Parser.ENUMERATED);
    tagNames.put("0:ab", "mscOutgoingROUTE");
    tagTypes.put("0:ab", ASN1Parser.ENUMERATED);
    tagNames.put("0;ac", "location");
    tagTypes.put("0;ac", ASN1Parser.SEQUENCE); 
    tagNames.put("0:9f814d", "callType");
    tagTypes.put("0:9f814d", ASN1Parser.ENUMERATED);
    tagNames.put("0;9f8168",  "recordNumber");
    tagTypes.put("0;9f8168",  ASN1Parser.INTEGER);
    tagNames.put("0;9f8233",  "ringingDuration");
    tagTypes.put("0;9f8233",  ASN1Parser.INTEGER);
    
    // MT CDR
    cdrNames.put("0", "MT");
    tagNames.put("0;81", "servedIMSI");
    tagTypes.put("0;81", ASN1Parser.BCDStringLE);
    tagNames.put("0;82", "servedIMEI");
    tagTypes.put("0;82", ASN1Parser.BCDStringLE);
    tagNames.put("0;84",  "callingNumber");
    tagTypes.put("0;84", ASN1Parser.BCDStringLE);
    tagNames.put("0;a9", "location");
    tagTypes.put("0;a9", ASN1Parser.SEQUENCE); 
    tagNames.put("0:9f814d", "calledNumber");
    tagTypes.put("0:9f814d", ASN1Parser.BCDStringLE);
    tagNames.put("0:9f814c", "alertingTime");
    tagTypes.put("0:9f814c", ASN1Parser.BCDStringLE);
    tagNames.put("0:9f8150", "callType");
    tagTypes.put("0:9f8150", ASN1Parser.ENUMERATED);
    tagNames.put("0:9f8175", "translatedNumber");
    tagTypes.put("0:9f8175", ASN1Parser.BCDStringLE);
  

    
  }

  @Override
  public String getTagName(String tagPath)
  {
    if (tagNames.containsKey(tagPath))
    {
      return tagNames.get(tagPath);
    }
    else
    {
      return "";
    }
  }
  
  public String getCDRName(String tagPath)
  {
    if (cdrNames.containsKey(tagPath))
    {
      return cdrNames.get(tagPath);
    }
    else
    {
      return "Unknown (" + tagPath + ")";
    }
  }
  
  @Override
  public int getTagType(String tagPath)
  {
    if (tagTypes.containsKey(tagPath))
    {
      return tagTypes.get(tagPath);
    }
    else
    {
      return -1;
    }
  }
}

    

