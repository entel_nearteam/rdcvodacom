/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EnterTeleProbe.Decoding;
import OpenRate.adapter.file.BinaryFileInputAdapter;
import OpenRate.record.IRecord;
import OpenRate.record.RatingRecord;
import java.util.ArrayList;
import OpenRate.parser.ASN1Parser;
import OpenRate.parser.Asn1Class;
import OpenRate.record.FlatRecord;
import java.util.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hmarikan
 */
public class AlcatelRCPInputAdapter extends BinaryFileInputAdapter {
    
    
/**
 * Binary input file adapter. As the files are not that large, we attempt to
 * parse them all in one go. This means that we load the entire binary file
 * contents into memory and then run through it decoding. This gives us a huge
 * performance boost over random access reading.
 */
  // this is the CVS version info
  public static String CVS_MODULE_INFO="OpenRate, $RCSfile: BalInputAdapter.java,v $, $Revision: 1.1 $, $Date: 2012/01/29 13:31:51 $";

  // This is the object that is used to compress the records
  RatingRecord tmpDataRecord = null;

  // Defintion of the tags and so on
  AlcatelSSPDef asn1Specification = new AlcatelSSPDef();
  
  // our ASN1 parser object
  //ASN1Parser asnp;
  
  // -----------------------------------------------------------------------------
  // ------------------ Start of inherited Plug In functions ---------------------
  // -----------------------------------------------------------------------------

 /**
  * This is called when the synthetic Header record is encountered, and has the
  * meaning that the stream is starting. In this example we have nothing to do
  * 
  * @param r The record we are working on
  * @return The processed record
  */
  @Override
  public IRecord procHeader(IRecord r)
  {
    asn1Specification.initTags();
    //asnp = new ASN1Parser(asn1Specification);
  
    return r;
  }

 /**
  * This is called when a data record is encountered. You should do any normal
  * processing here. For the input adapter, we probably want to change the 
  * record type from FlatRecord to the record(s) type that we will be using in
  * the processing pipeline.
  *
  * This is also the location for accumulating records into logical groups
  * (that is records with sub records) and placing them in the pipeline as
  * they are completed. If you receive a sub record, simply return a null record
  * in this method to indicate that you are handling it, and that it will be
  * purged at a later date.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procValidRecord(IRecord r)
  {
    return r;
  }

 /**
  * This is called when a data record with errors is encountered. You should do
  * any processing here that you have to do for error records, e.g. statistics,
  * special handling, even error correction!
  * 
  * The input adapter is not expected to provide any records here.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procErrorRecord(IRecord r)
  {
    return r;
  }

 /**
  * This is called when the synthetic trailer record is encountered, and has the
  * meaning that the stream is now finished. In this example, all we do is 
  * pass the control back to the transactional layer.
  *
  * In models where record aggregation (records and sub records) is used, you
  * might want to check for any purged records here.
  * 
  * @param r The record we are working on
  * @return The processed record
    */
  @Override
  public IRecord procTrailer(IRecord r)
  {
    return r;
  }

 /**
  * This is where the binary parsing happens.
  * 
  * @param fileContents The contents of the binary file
  * @return An array of records to push into the pipe
  */
  @Override
  public ArrayList<IRecord> parseBinaryFileContents(byte[] fileContents) {
    ArrayList<IRecord> outBatch = new ArrayList<>();
    ////System.out.println("File Length  : " + fileContents.length);
 
    ASN1Parser asnp = new ASN1Parser(asn1Specification);
    int recLen = 0;
    String version;
    byte[] lenBuffer = new byte[2];
    byte [] buffer;
    byte [] tmpBuffer;
    IRecord tmpRecord;
    int i = 0;
    int currentBytePos = 0;
    while (fileContents.length > currentBytePos ) {
        recLen = 0;
                
        lenBuffer[0] = fileContents[currentBytePos];
             
        recLen |= (0x000000FF &  lenBuffer[0]);
        tmpBuffer = new byte[1];
        tmpBuffer[0] =  fileContents[currentBytePos+1];
        currentBytePos = currentBytePos+1;
        version = asnp.parseBCDString(tmpBuffer);
        
              
        if(!version.equalsIgnoreCase("00")) {
            recLen = 0;
            lenBuffer[1] = fileContents[currentBytePos+1];
                              
            recLen = 0;
            recLen |= ((0x000000FF & lenBuffer[1]) << 8) | (0x000000FF & lenBuffer[0]);
                        
            currentBytePos = currentBytePos+1;
           
            
        }        
        buffer = new byte[recLen];
        i = 1;
        while (i < recLen) {
             
             buffer[i-1] = fileContents[ currentBytePos + i++];
            
        }
        currentBytePos = i + currentBytePos;
        
     
        try {
            tmpRecord = processCDR(buffer, version);
            if (tmpRecord != null)
			{
            	outBatch.add(tmpRecord);
			}
        } catch (Exception ex) {
            //System.out.println("Unable to Parse Record  : " + ex );
            Logger.getLogger(AlcatelRCPInputAdapter.class.getName()).log(Level.SEVERE, null, ex);
       
        }
                  
    }
      
    return outBatch;
  }
  
 /**
  * process the Header block.
  * 
  * @param cdrBlock the CDR block to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private int processHeaderBlock(byte[] headerBlock) throws Exception
  {
    Asn1Class output;
    
    return 0;
  }
  
 /**
  * process the Trailer block.
  * 
  * @param cdrBlock the CDR block to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private int processTrailerBlock(byte[] trailerBlock) throws Exception
  {
    Asn1Class output;
    
    return 0;
  }
  
 /**
  * process the CDR block - this is the collection of all the CDRs in a file.
  * This method unpacks the CDR list into individual CDRs so they can be
  * processed.
  * 
  * @param cdrBlock the CDR block to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private ArrayList<IRecord> processCDRBlock(byte[] cdrBlock) throws Exception
  {
    Asn1Class output;
    ArrayList<IRecord> outBatch = new ArrayList<>();
    return outBatch;
  }

 /**
  * process the CDR.
  * 
  * @param cdrRecord the CDR to process
  * @return the number of CDRs
  * @throws Exception 
  */
  private IRecord processCDR(byte[] buffer, String versionStr) throws Exception
  {
    StringBuilder recordContents = new StringBuilder();
    ASN1Parser asnpr = new ASN1Parser(asn1Specification);
    
    ////System.out.println("processing CDR\n"); 
    
    recordContents.append("Version=" ).append (versionStr).append(";");
    //MSC Type + call Type
    byte[] tmpBuffer = new byte[1];
    int lasttBytePos = 0;
       
    tmpBuffer[0] = buffer[0];
       
    int mscType = ((tmpBuffer[0] & 0x70) >> 4);
    int callType = ((tmpBuffer[0] & 0x0f));
    
    recordContents.append("MSC Type=" ).append (mscType).append(";");
    recordContents.append("Call Type=" ).append (callType).append(";");     
    
    if (!(callType == 0 || callType == 1))
	{
		return null;
	}
    
    tmpBuffer[0] =  buffer[1];
    String reason = asnpr.parseBytes(tmpBuffer);
   
    
    recordContents.append("Efficiency indicator Successful call termination type=" ).append (reason).append(";");
   
    
    tmpBuffer[0] = buffer[2];
                      
    String ocall = asnpr.parseBytes(tmpBuffer);
    recordContents.append("Unefficiency cause=" ).append (ocall).append(";");
      
    
    tmpBuffer[0] = buffer[3];
    int callOrigInd = ((tmpBuffer[0] & 0x70) >> 4);
    int chargingInd = ((tmpBuffer[0] & 0x0f));
    
    recordContents.append("Call Origin Indicator=" ).append (callOrigInd).append(";");
    recordContents.append("Charging Indicatore=" ).append (chargingInd).append(";"); 
    
    tmpBuffer[0] = buffer[4];
    String ts = asnpr.parseBytes(tmpBuffer);
    recordContents.append("TeleService Ind=" ).append (ts).append(";");  
    
    tmpBuffer[0] = buffer[5];
    String bs = asnpr.parseBytes(tmpBuffer);
    recordContents.append("BearerService=" ).append (bs).append(";"); 
    
    //circuit time
    tmpBuffer[0] = buffer[6];
    String year = asnpr.parseBCDString(tmpBuffer);
   
    
    tmpBuffer[0] = buffer[7];
    String month = asnpr.parseBCDString(tmpBuffer);
    
    tmpBuffer[0] = buffer[8];
    String day = asnpr.parseBCDString(tmpBuffer);
   
    tmpBuffer[0] = buffer[9];
    String hr = asnpr.parseBCDString(tmpBuffer);
       
    tmpBuffer[0] = buffer[10];
    String min = asnpr.parseBCDString(tmpBuffer);
    
    tmpBuffer[0] = buffer[11];;
    String sec = asnpr.parseBCDString(tmpBuffer);
                
    //System.out.println(" Circuit allocation time  : " + year + month + day + hr + min + sec) ;
       
    recordContents.append("Allocation time=" ).append (year).append (month).append (day).append (hr).append (min).append (sec).append(";"); 
    
    
    
    byte[] dur = new byte[3];
    tmpBuffer[0] = buffer[12];;
    
    int j = 0;
    for (int di = 12; di <= 14; di++) {
        
        dur[j++] = buffer[di];
              
    }
     
    int duration = 0;
       
    duration = asnpr.parseIntegerAsInteger(dur);
    
    //System.out.println(" Duration  : " +duration);

    recordContents.append("Duration=" ).append (duration).append(";"); 
    
    
    tmpBuffer[0] = buffer[15];
    month = asnpr.parseBCDString(tmpBuffer);
    
    tmpBuffer[0] = buffer[16];
    day = asnpr.parseBCDString(tmpBuffer);
   
    tmpBuffer[0] = buffer[17];
    hr = asnpr.parseBCDString(tmpBuffer);
       
    tmpBuffer[0] = buffer[18];
    min = asnpr.parseBCDString(tmpBuffer);
    
    tmpBuffer[0] = buffer[19];
    sec = asnpr.parseBCDString(tmpBuffer);
    
    //System.out.println(" call end time  : " +  month + day + hr + min + sec) ;
    
    recordContents.append("Call End Time=" ).append (month).append (day).append (hr).append (min).append (sec).append(";"); 
    
    
    //mobile station identity IMEI
    tmpBuffer[0] = buffer[20];
    int  MsIdLen = 0;
    MsIdLen   |= (0x000000FF & tmpBuffer[0]);
  
    //System.out.println(" IMEI len: " +  MsIdLen) ;
    lasttBytePos = 20;
        
    if(MsIdLen != 0) {
    
        byte[] nTmpBuffer = new byte[MsIdLen];
       
        for(int i = 0; i < MsIdLen; i++) {
           nTmpBuffer[i] = buffer[++lasttBytePos];
        }
    
        String imei = parseTBCDString(nTmpBuffer);
        //System.out.println(" IMEI  : " +  imei);
        recordContents.append("IMEI=" ).append (imei).append(";"); 
    
    }
                    
          
            
    //List of service components
    tmpBuffer[0] = buffer[++lasttBytePos];
    MsIdLen = 0;
    MsIdLen   |= (0x000000FF & tmpBuffer[0]);
    
    lasttBytePos +=  MsIdLen;
    
    //read the link information bytes
    
    tmpBuffer[0] = buffer[++lasttBytePos];
    MsIdLen = 0;
    MsIdLen   |= (0x000000FF & tmpBuffer[0]);
    
    lasttBytePos +=  MsIdLen;
    
    
    //mobile subscriber identity
       
    tmpBuffer[0] = buffer[++lasttBytePos];
    MsIdLen = 0;
    MsIdLen   |= (0x000000FF & tmpBuffer[0]);
    //lasttBytePos +=  MsIdLen;
    
     if(MsIdLen != 0) {
    
        byte[] nTmpBuffer = new byte[MsIdLen];
       
       
        for(int i = 0; i < MsIdLen; i++) {
          
            nTmpBuffer[i] = buffer[++lasttBytePos];
        }
    
        String imsi = asnpr.parseBytes(nTmpBuffer);
        
        //System.out.println("IMSI " + imsi + " lenghth "+ MsIdLen );
        recordContents.append("IMSI=" ).append (imsi).append(";"); 
    
    }
    
     
     
    //MSC identity
    
    tmpBuffer[0] = buffer[++lasttBytePos];
    MsIdLen = 0;
    MsIdLen   |= (0x000000FF & tmpBuffer[0]);
    
          
    if(MsIdLen != 0) {
    
        byte[] nTmpBuffer = new byte[MsIdLen - 1];
       
        byte[] singleByteBuffer = new byte[1];
        for(int i = 0; i < MsIdLen; i++) {
            if(i == 0 ) {
                singleByteBuffer[0] = buffer[++lasttBytePos];
                continue;
            }
            nTmpBuffer[i - 1] = buffer[++lasttBytePos];
        }
    
        String mscIdentity = parseTBCDString(nTmpBuffer);
        int addressInd = ((singleByteBuffer[0] & 0x70) >> 4);
        int numberingPlan = ((singleByteBuffer[0] & 0x0f));
        recordContents.append("MSC Identity Nature Indicator= " ).append (addressInd).append(";");
        recordContents.append("MSC Identity Numbering Plan Indicator= " ).append (numberingPlan).append(";");
    
        recordContents.append("MSC Identity=" ).append (mscIdentity).append(";"); 
    
    }
  
    
    //call partner identity
    tmpBuffer[0] = buffer[++lasttBytePos];
    MsIdLen = 0;
    MsIdLen   |= (0x000000FF & tmpBuffer[0]);
    
    byte[] nTmpBuffer = new byte[MsIdLen - 1];
    byte[] singleByteBuffer = new byte[1];
    for(int i = 0; i < MsIdLen; i++) {
        if(i == 0 ) {
            singleByteBuffer[0] = buffer[++lasttBytePos];
            continue;
        }
        nTmpBuffer[i - 1] = buffer[++lasttBytePos];
    }
    
    
    String calledParty = parseTBCDString(nTmpBuffer);
    int addressInd = ((singleByteBuffer[0] & 0x70) >> 4);
    int numberingPlan = ((singleByteBuffer[0] & 0x0f));
    recordContents.append("Call Partner Address Indicator= " ).append (addressInd).append(";");
    recordContents.append("Call Partner Numbering Plan Indicator= " ).append (numberingPlan).append(";");
    
    recordContents.append("Call Partner=" ).append (calledParty).append(";"); 
    ////System.out.println(" Called Party   : " + asnpr.parseBCDString(nTmpBuffer));
    
    //MS Location Identity
    tmpBuffer[0] = buffer[++lasttBytePos];
    MsIdLen = 0;
    MsIdLen   |= (0x000000FF & tmpBuffer[0]);
    
    if(MsIdLen != 0) {
        nTmpBuffer = new byte[MsIdLen - 1];
   
    
        for(int i = 0; i < MsIdLen; i++) {
            if(i == 0 ) {
                singleByteBuffer[0] = buffer[++lasttBytePos];
                continue;
            }
            nTmpBuffer[i - 1] = buffer[++lasttBytePos];
        }
    
        addressInd = ((singleByteBuffer[0] & 0x70) >> 4);
        numberingPlan = ((singleByteBuffer[0] & 0x0f));
        String callingParty = parseTBCDString(nTmpBuffer);
//        recordContents.append("(MSLOC)Calling Party Address Indicator= " ).append (addressInd).append(";");
//        recordContents.append("((MSLOC) Calling Party Numbering Plan Indicator= " ).append (numberingPlan).append(";");
        recordContents.append("(MSLOC)Calling Party=" ).append (callingParty).append(";"); 
    }
    
    
    //MS Location Identity Extension - Not Used
     
    //Required Bearer capabilities
    
    tmpBuffer[0] = buffer[++lasttBytePos];
    MsIdLen = 0;
    MsIdLen   |= (0x000000FF & tmpBuffer[0]);
    
    ////System.out.println(" MS Location Identity Extension  len : " + MsIdLen);
    
    if(MsIdLen != 0) {
        nTmpBuffer = new byte[MsIdLen];
       
        for(int i = 0; i < MsIdLen; i++) {
                nTmpBuffer[i] = buffer[++lasttBytePos];
        }
    
        String requireBearedCap =  asnpr.parseBytes(nTmpBuffer);
        recordContents.append("Required bearer capability=" ).append (requireBearedCap).append(";");
    }
    /*
    //Outgoing Trunk Group Identity
    
    tmpBuffer[0] = buffer[++lasttBytePos];
    MsIdLen = 0;
    MsIdLen   |= (0x000000FF & tmpBuffer[0]);
    
       
    if(MsIdLen != 0) {
        nTmpBuffer = new byte[MsIdLen];
       
        for(int i = 0; i < MsIdLen; i++) {
                nTmpBuffer[i] = buffer[++lasttBytePos];
        }
    
        String MsLocationId =  asnpr.parseBytes(nTmpBuffer);
        recordContents.append("Outgoing Trunk Group=" ).append (hexToAscii(MsLocationId)).append(";");
    }
     * 
     * */
    
    
     //Incoming Trunk Group Identity
    
    tmpBuffer[0] = buffer[++lasttBytePos];
    MsIdLen = 0;
    MsIdLen   |= (0x000000FF & tmpBuffer[0]);
    
    ////System.out.println(" Incoming Trunk Group  len : " + MsIdLen);
    
    if(MsIdLen != 0) {
        nTmpBuffer = new byte[MsIdLen];
        int ITGLen = 0;
        ITGLen   |= (0x000000FF & buffer[++lasttBytePos]);
        
        for(int i = 0; i < ITGLen; i++) {
                nTmpBuffer[i] = buffer[++lasttBytePos];
        }
       
                
        String incomingTrunkGroup = asnpr.parseBytes(nTmpBuffer);
        recordContents.append("Incoming Trunk Group=" ).append (hexToAscii(incomingTrunkGroup)).append(";");
       
        
        
    }
    
    //System.out.println("NEW RCP CDR : " + recordContents.toString());
    
    FlatRecord tmpRecord = new FlatRecord(recordContents.toString());
    
    return tmpRecord;
  }  
  

   

public String parseTBCDString(byte[] value) {
		StringBuilder buf = new StringBuilder(value.length * 2);

		for (int i = 0; i < value.length; ++i) {
      int hiNibble = ((value[i] & 0xf0) >> 4);
      int loNibble = (value[i] & 0x0f);
			if ((i != value.length) && (loNibble != 0x0f)) // if not pad char
  			buf.append((char) (loNibble + '0'));
			if ((i != value.length) && (hiNibble != 0x0f)) // if not pad char
				buf.append((char) (hiNibble + '0'));
		}
		return buf.toString();
    }


public String hexToAscii(String str) {
  int n = str.length();
  StringBuilder sb = new StringBuilder(n / 2);
  for (int i = 0; i < n; i += 2) {
    char a = str.charAt(i);
    char b = str.charAt(i + 1);
    sb.append((char) ((hexToInt(a) << 4) | hexToInt(b)));
  }
    return sb.toString();
}

private static int hexToInt(char ch) {
  if ('a' <= ch && ch <= 'f') { return ch - 'a' + 10; }
  if ('A' <= ch && ch <= 'F') { return ch - 'A' + 10; }
  if ('0' <= ch && ch <= '9') { return ch - '0'; }
  throw new IllegalArgumentException(String.valueOf(ch));
}


    
}
