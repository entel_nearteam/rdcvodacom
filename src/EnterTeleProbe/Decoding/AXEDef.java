/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Limited 2006-2012.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of architecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are discplaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
package EnterTeleProbe.Decoding;

import OpenRate.parser.ASN1Parser;
import OpenRate.parser.IASN1Def;
import java.util.HashMap;

/**
 *
 * @author ian
 */
public class AXEDef implements IASN1Def
{
  // These control the decoding and naming of the fields
  private HashMap<String,String> tagNames;
  private HashMap<String,Integer> tagTypes;
  private HashMap<String,String> cdrNames;
  
  @Override
  public void initASN1() {
  }

 /**
  * Initialise the tags for the decoding. The key into the hash maps is 
  * CDRtype hex;Tag ID hex[;Block ID Hex]
  */
  @Override
  public void initTags() {
    // Set up the hash maps
    tagNames = new HashMap<>();
    tagTypes = new HashMap<>();
    cdrNames = new HashMap<>();
    
    // CDR Types
    cdrNames.put("0", "Transit");
    cdrNames.put("1", "MOC");
    cdrNames.put("2", "RCF");
    cdrNames.put("3", "CF");
    cdrNames.put("4", "MTC");
    cdrNames.put("5", "SMMO");
    cdrNames.put("6", "SMMO");
    cdrNames.put("7", "SMMT");
    cdrNames.put("8", "SMMT");
    cdrNames.put("9", "SUPS");
    cdrNames.put("13","Transit");
    cdrNames.put("14","INI");
    cdrNames.put("15","INE");
    cdrNames.put("17","PTC");
    cdrNames.put("18","PTO");
    cdrNames.put("19","PTS");
    cdrNames.put("21","CHG");
    cdrNames.put("24","LOC"); 
    
    // MOC mapped fields
    tagNames.put("1;1", "Filename");
    tagTypes.put("1;1", ASN1Parser.INTEGER);
    tagNames.put("1;4", "A_Num");
    tagTypes.put("1;4", ASN1Parser.BCDString);
    tagNames.put("1;5", "IMSI");
    tagTypes.put("1;5", ASN1Parser.BCDString);
    tagNames.put("1;6", "IMEI");
    tagTypes.put("1;6", ASN1Parser.BCDString);
    tagNames.put("1;7", "B_Num");
    tagTypes.put("1;7", ASN1Parser.BCDString);
    tagNames.put("1;9", "AnswerDate");
    tagTypes.put("1;9", 0xF1);
    tagNames.put("1;10", "AnswerTime");
    tagTypes.put("1;10", 0xF2);
    tagNames.put("1;12", "Duration");
    tagTypes.put("1;12", 0xF3);
    tagNames.put("1;22", "OutTrunk");
    tagTypes.put("1;22", ASN1Parser.IA5STRING);
    tagNames.put("1;23", "InTrunk");
    tagTypes.put("1;23", ASN1Parser.IA5STRING);
    tagNames.put("1;67", "SwitchID");
    tagTypes.put("1;67", 0xF5);
    tagNames.put("1;27", "CellIdLac");
    tagTypes.put("1;27", 0xF4);
//    
//    IDX_Filename        
//IDX_Call_Type       
//IDX_Imsi            
//IDX_Imei            
//IDX_Served_MSISDN   
//IDX_A_Num           
//IDX_B_Num           
//IDX_Orig_Trk        
//IDX_Dest_Trk        
//IDX_LAC             
//IDX_Cell_Identity   
//IDX_Orig_Dt         
//IDX_Bill_Dur        
//IDX_RecordLength    
//    
    // MTC mapped fields
    tagNames.put("4;1", "CallReferenceId");
    tagTypes.put("4;1", ASN1Parser.INTEGER);
    tagNames.put("4;4", "CallingPartyNumber");
    tagTypes.put("4;4", ASN1Parser.BCDString);
    tagNames.put("4;5", "CalledPartyNumber");
    tagTypes.put("4;5", ASN1Parser.BCDString);
    tagNames.put("4;36", "originalCalledNumber");
    tagTypes.put("4;36", ASN1Parser.BCDString);
    tagNames.put("4;6", "IMSI");
    tagTypes.put("4;6", ASN1Parser.BCDString);
    tagNames.put("4;7", "IMEI");
    tagTypes.put("4;7", ASN1Parser.BCDString);
    tagNames.put("4;8", "MSRN");
    tagTypes.put("4;8", ASN1Parser.BCDString);
    tagNames.put("4;10", "AnswerDate");
    tagTypes.put("4;10", 0xF1);
    tagNames.put("4;11", "AnswerTime");
    tagTypes.put("4;11", 0xF2);
    tagNames.put("4;13", "Duration");
    tagTypes.put("4;13", 0xF3);
    tagNames.put("4;66", "SwitchID");
    tagTypes.put("4;66", 0xF5);
    tagNames.put("4;22", "OutTrunk");
    tagTypes.put("4;22", ASN1Parser.IA5STRING);
    tagNames.put("4;23", "InTrunk");
    tagTypes.put("4;23", ASN1Parser.IA5STRING);
    tagNames.put("4;27", "CellIdLac");
    tagTypes.put("4;27", 0xF4);
  }

  @Override
  public String getTagName(String tagPath)
  {
    if (tagNames.containsKey(tagPath))
    {
      return tagNames.get(tagPath);
    }
    else
    {
      return "";
    }
  }
  
  public String getCDRName(String tagPath)
  {
    if (cdrNames.containsKey(tagPath))
    {
      return cdrNames.get(tagPath);
    }
    else
    {
      return "Unknown (" + tagPath + ")";
    }
  }
  
  @Override
  public int getTagType(String tagPath)
  {
    if (tagTypes.containsKey(tagPath))
    {
      return tagTypes.get(tagPath);
    }
    else
    {
      return -1;
    }
  }
}
