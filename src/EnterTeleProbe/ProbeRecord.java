/* ====================================================================
 * Limited Evaluation License:
 *
 * The exclusive owner of this work is Tiger Shore Management Ltd.
 * This work, including all associated documents and components
 * is Copyright Tiger Shore Management Ltd 2006-2012.
 *
 * The following restrictions apply unless they are expressly relaxed in a
 * contractual agreement between the license holder or one of its officially
 * assigned agents and you or your organisation:
 *
 * 1) This work may not be disclosed, either in full or in part, in any form
 *    electronic or physical, to any third party. This includes both in the
 *    form of source code and compiled modules.
 * 2) This work contains trade secrets in the form of arhitecture, algorithms
 *    methods and technologies. These trade secrets may not be disclosed to
 *    third parties in any form, either directly or in summary or paraphrased
 *    form, nor may these trade secrets be used to construct products of a
 *    similar or competing nature either by you or third parties.
 * 3) This work may not be included in full or in part in any application.
 * 4) You may not remove or alter any proprietary legends or notices contained
 *    in or on this work.
 * 5) This software may not be reverse-engineered or otherwise decompiled, if
 *    you received this work in a compiled form.
 * 6) This work is licensed, not sold. Possession of this software does not
 *    imply or grant any right to you.
 * 7) You agree to disclose any changes to this work to the copyright holder
 *    and that the copyright holder may include any such changes at its own
 *    discretion into the work
 * 8) You agree not to derive other works from the trade secrets in this work,
 *    and that any such derivation may make you liable to pay damages to the
 *    copyright holder
 * 9) You agree to use this software exclusively for evaluation purposes, and
 *    that you shall not use this software to derive commercial profit or
 *    support your business or personal activities.
 *
 * This software is provided "as is" and any expressed or impled warranties,
 * including, but not limited to, the impled warranties of merchantability
 * and fitness for a particular purpose are discplaimed. In no event shall
 * Tiger Shore Management or its officially assigned agents be liable to any
 * direct, indirect, incidental, special, exemplary, or consequential damages
 * (including but not limited to, procurement of substitute goods or services;
 * Loss of use, data, or profits; or any business interruption) however caused
 * and on theory of liability, whether in contract, strict liability, or tort
 * (including negligence or otherwise) arising in any way out of the use of
 * this software, even if advised of the possibility of such damage.
 * This software contains portions by The Apache Software Foundation, Robert
 * Half International.
 * ====================================================================
 */
/* ========================== VERSION HISTORY =========================
 * $Log: ProbeRecord.java,v $
 * Revision 1.21  2012-07-25 16:59:03  jean-baptiste
 * new time rounding + agg
 *
 * Revision 1.20  2012-07-17 16:37:01  jean-baptiste
 * New date calculation and A number modification
 *
 * Revision 1.19  2012-07-12 15:43:55  ian
 * Robustness
 *
 * Revision 1.18  2012-07-12 06:01:37  ian
 * Better date management
 *
 * Revision 1.17  2012-07-12 05:25:46  ian
 * Code review
 *
 * Revision 1.16  2012-07-08 13:17:35  ian
 * Refactor
 *
 * Revision 1.15  2012-06-27 16:25:36  jean-baptiste
 * - PDD added in probe cdr + SQL structure
 *
 * Revision 1.14  2012-06-19 22:36:07  ian
 * Add recycle processing
 *
 * Revision 1.13  2012-06-18 15:58:25  jean-baptiste
 * Added Operator info in aggregation
 * Needed to know which probe the cdr comes from
 *
 * Revision 1.12  2012-06-13 08:48:54  jean-baptiste
 * added Pending Duration Delay enrichment
 *
 * Revision 1.11  2012-06-06 14:51:10  jean-baptiste
 * added Event date for non connected calls                             (cf. Probe/ProbeRecord.java)
 * removed the ".000" after the Event time                               (cf. Probe/ProbeRecord.java)
 * finished the Quintum cdr settings                                           (cf. Probe DB data + ProbeRecord.java)
 *
 * Revision 1.10  2012-05-22 12:37:00  jean-baptiste
 * Loader Record for Cyrille
 *
 * Revision 1.9  2012-04-04 17:19:36  ian
 * Add Connection Count indicator for ASR
 *
 * Revision 1.8  2012/03/24 14:32:55  ian
 * Add obfuscation, minor improvements
 *
 * Revision 1.7  2012/01/26 09:13:27  ian
 * Update for filtering
 *
 * Revision 1.6  2012/01/20 20:36:25  ian
 * Correct time format
 *
 * Revision 1.5  2012/01/19 17:23:02  ian
 * Add aggregation and CDR output
 *
 * Revision 1.4  2012/01/18 12:35:56  ian
 * Add normlisation and zoning
 *
 * Revision 1.3  2012/01/17 12:54:51  ian
 * Add trunk mapping
 *
 * Revision 1.2  2012/01/17 11:55:13  ian
 * Map CS and IGW types
 *
 * Revision 1.1  2012/01/16 22:51:53  ian
 * Initial Starting Version
 *
 * ====================================================================
 */
package EnterTeleProbe;

import OpenRate.record.ErrorType;
import OpenRate.record.RatingRecord;
import OpenRate.record.RecordError;
import OpenRate.utils.ConversionUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 */
public class ProbeRecord extends RatingRecord
{
	// Field Splitter in the records
	public static final String FIELD_SPLITTER = ",";
	public final static String HEADER_TRAILER_ID = "NotExpected";

	// Used for managing recycling
	public static final String RECYCLE_TAG = "ORRECYCLE";

	// --------------------- Tenor CDR ---------------------
	// index offsets for the Tenor CDR input fields (starts at 0, not 1)
	public final static int IDX_Orig_Dt = 0;
	public final static int IDX_A_Num = 1;
	public final static int IDX_B_Cc = 2;
	public final static int IDX_B_Intl_Ind = 3;
	public final static int IDX_B_Num = 4;
	public final static int IDX_Bill_Dur = 5;
	public final static int IDX_Call_Type = 6;
	public final static int IDX_Called_Imsi = 7;
	public final static int IDX_Calling_Imsi = 8;
	public final static int IDX_Carrier_Id = 9;
	public final static int IDX_Cdrrecordtype = 10;
	public final static int IDX_Cell_Identity = 11;
	public final static int IDX_Dest_Trk = 12;
	public final static int IDX_Imei = 13;
	public final static int IDX_Orig_Trk = 14;
	public final static int IDX_Call_Direction = 15;
	public final static int IDX_Source_Indicator = 16;
	public final static int IDX_Switch_ID = 17;
	public final static int IDX_Filename = 18;
	public final static int IDX_LAC = 19;
	public final static int IDX_RecordLength = 19;

	// public final static int IDX_CS_callDuration_start = 245;
	// public final static int IDX_CS_callDuration_length = 10;

	// parameters for unused types (may be used in other formats)
	public final static int HEADER_RECORD_FIELDS = 0;
	public final static int TRAILER_RECORD_FIELDS = 0;

	// Record Types
	public final static int HEADER_RECORD = 10;
	public final static int PROBE_RECORD = 20;
	public final static int PROFILE_RECORD = 30;
	public final static int LIVE_TRACK_RECORD = 40;
	public final static int TRAILER_RECORD = 90;

	// Working variables from CDR
	public String recordType; // Record type from the input
	public String MSISDN; // Served MSISDN
	public String callingNumber; // A Number
	public String calledNumber; // B Number
	public String translatedNumber; // Not sure
	public String inOperator; // In trunk code
	public String outOperator; // Out trunk code
	// String answerTime; // Call Start Time
	public String callDuration; // Duration
	public String Operator; // Operator sending the cdr
	// Enrichment variables
	public String cdrID; // CDR ID used for errors

	public String mapGroup; // map group ID used for lookups in later processing
	public String inTrunkName; // Trunk ID used for information
	public String outTrunkName; // Trunk ID used for information
	public String inTrunkId; // Trunk ID used for aggregation
	public String outTrunkId; // Trunk ID used for aggregation
	public String trunkType; // Trunk type, International or National
	public String profileType; // Profile type, Incoming or Outgoing
	public String normCalledNumber; // Called number after normalisation
	public String normCallingNumber; // Calling number after normalisation
	public String matchCalledNumber; // Called number after obfuscation
										// (optional)
	public String matchCallingNumber; // Calling number after obfuscation
										// (optional)
	public String destination; // Zone lookup destination
	public String category; // Zone lookup category
	public String recordDate; // date of the call in output format
	public String recordTime; // date of the call in output format
	public String processDate; // date of CDR processing
	public String connectTime; // used for PDD calculation
	public boolean probeRecord; // True if this record is a proble record
	public boolean secondRecord = false; // True if this record is processed for
											// the 2nd time, no agg needed in
											// this case
	public long duration; // Numeric representation of the duration
	public long callConnected; // Indicator if the call was connected 1 =
								// connected, 0 = not connected
	public long PDD; // Pending Duration Delay
	public String LAC; // Location Area Code
	public String roundedDate5; // |
	public Date tmpDate; // | => used for 5min time rounding
	public String[] tmpvar; // |
	public String tmpString; // |
	public String profiledNumber; // Number communicated for profiling analysis
	public long effTS; // Timestamp arrondi pour Live reconciliation
	public long timezone; // Decalage horraire
	public String switchID;
	public String IMSI;
	public String IMEI;
	public String BTS;
	public String CallType;
	public String DestinationID;
	public String Origin;
	public String OriginID;
	public String tmpDest;
	public String tmpPref;
	public String TrunkTypeID;
	public String CallTypeID;

	// File Control variables
	public String recordId;
	public String fileName;
	public int fileNameID;
	public int recycleCount = 0;

	// Output Control variables
	public boolean GoodOutput = true;
	public boolean DiscardOutput = true;
	public boolean isProbeRecord = false;
	public int usesNewTrunk = 0; // utilisé comme nombre binaire (00,01,10,11)
									// avec 1er digit= in_trunk et 2nd digit
									// =out_trunk
	public boolean isNewFilename = false;

	// Trailer counters - only used in trailer
	public long FileRecordCounterGood = 0;
	public long FileRecordCounterDiscard = 0;
	public double FileCostGood = 0;
	public double FileCostDiscard = 0;
	public double subRecordCost = 0;
	public long subRecordCount = 0;

	// Used for calculating the validites of the counters
	private ConversionUtils conv = new ConversionUtils();

	/**
	 * Default Constructor for RateRecord, creating the empty record container
	 */
	public ProbeRecord()
	{
		super();
		conv.setInputDateFormat("yyyyMMddHHmmss");
	}

	/**
	 * Utility function to map a file header or trailer record
	 * 
	 * @param dataToMap
	 *            Data from the input file to be mapped
	 */
	public void mapHeaderTrailerRecord(String dataToMap)
	{
		// It is a header or a trailer - we will know by counting the fields
		fields = dataToMap.split(ProbeRecord.FIELD_SPLITTER);
		OriginalData = dataToMap;

		if (fields.length == ProbeRecord.HEADER_RECORD_FIELDS)
		{
			RECORD_TYPE = HEADER_RECORD;
		} else if (fields.length == ProbeRecord.TRAILER_RECORD_FIELDS)
		{
			RECORD_TYPE = TRAILER_RECORD;
		}
	}

	/**
	 * Utility function to map detail record
	 * 
	 * @param dataToMap
	 *            Data from the input file to be mapped
	 */

	public void mapRDCProbeRecord(String dataToMap)
	{
		// Date converter
		SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		SimpleDateFormat sdfOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		// Split record
		fields = dataToMap.split(FIELD_SPLITTER);

		// manage recycle
		if (fields[0].equals(RECYCLE_TAG))
		{
			// Get the recycle count
			recycleCount = Integer.valueOf(fields[2]);

			// Increment it
			recycleCount++;

			// Strip the header information off
			String[] newFields = new String[IDX_RecordLength];
			StringBuilder newOrigData = new StringBuilder();
			for (int index = 0; index < IDX_RecordLength; index++)
			{
				// Copy over
				newFields[index] = fields[index + 3];
				newOrigData.append(fields[index + 3]).append(FIELD_SPLITTER);
			}

			// Overwrite the old fields
			fields = newFields;
			setOriginalData(newOrigData.toString().replaceAll(";$", ""));
		}

		// Map basic input
		OriginalData = dataToMap;
		RECORD_TYPE = PROBE_RECORD;

		// Check we have a long enough string to work on
		if (fields.length >= IDX_RecordLength)
		{
			// parse the call type
			try
			{
				switch (Integer.parseInt(getField(IDX_Call_Type))) { // "1" = MO
																		// -
																		// Voice
																		// "2" =
																		// MT -
																		// Voice
																		// "3" =
																		// Call
																		// Forwarding
				case 1:
					CallType = "MO"; // Indice pour fichier aggregation
					CallTypeID = "1"; // Indice pour BDD
					break;
				case 2:
					CallType = "MT"; // Indice pour fichier aggregation
					CallTypeID = "2"; // Indice pour BDD
					break;
				case 3:
					CallType = "FW"; // Indice pour fichier aggregation
					CallTypeID = "3"; // Indice pour BDD
					break;
				}
			} catch (NumberFormatException nfe)
			{
				RecordError tmpError = new RecordError("ERR_INVALID_CALL_TYPE", ErrorType.DATA_VALIDATION);
				addError(tmpError);

				// leave - we don't need to process further
				return;
			}

			// Split up the data
			recordType = "Default";
			// recordType = getField(IDX_Call_type);
			// Conservation du B num original, on travaille par la suite avec
			// normCalledNumber
			calledNumber = getField(IDX_B_Num);
			if ((getField(IDX_B_Num).length() == 9)
					&& ((getField(IDX_B_Num).startsWith("8")) || (getField(IDX_B_Num).startsWith("9")) || (getField(IDX_B_Num).startsWith("1"))))
			{
				normCalledNumber = "243" + getField(IDX_B_Num);
			} else
			{
				normCalledNumber = getField(IDX_B_Num);
			}
			if ((getField(IDX_A_Num).length() == 9)
					&& ((getField(IDX_A_Num).startsWith("8")) || (getField(IDX_A_Num).startsWith("9")) || (getField(IDX_A_Num).startsWith("1"))))
			{
				callingNumber = "243" + getField(IDX_A_Num);
			} else
			{
				callingNumber = getField(IDX_A_Num);
			}
			// detection du tronc in puisque pas de portabilité de num
			if (callingNumber.startsWith("24397") || callingNumber.startsWith("24399"))
			{
				inOperator = "Airtel";
			} else if (callingNumber.startsWith("24384") || callingNumber.startsWith("24385"))
			{
				inOperator = "Orange";
			} else if (callingNumber.startsWith("24315") || callingNumber.startsWith("24312"))
			{
				inOperator = "Standard Telecom";
			} else if (callingNumber.startsWith("24389") || callingNumber.startsWith("24380"))
			{
				inOperator = "Tigo";
			} else if (callingNumber.startsWith("24382") || callingNumber.startsWith("24381"))
			{
				inOperator = "Vodacom";
			} else if (callingNumber.startsWith("24390"))
			{
				inOperator = "Africell";
			} else
			{
				inOperator = "Unknown";
			}

			// detection du tronc out puisque pas de portabilité de num
			if (normCalledNumber.startsWith("24397") || normCalledNumber.startsWith("24399"))
			{
				outOperator = "Airtel";
			} else if (normCalledNumber.startsWith("24384") || normCalledNumber.startsWith("24385"))
			{
				outOperator = "Orange";
			} else if (normCalledNumber.startsWith("24315") || normCalledNumber.startsWith("24312"))
			{
				outOperator = "Standard Telecom";
			} else if (normCalledNumber.startsWith("24389") || normCalledNumber.startsWith("24380"))
			{
				outOperator = "Tigo";
			} else if (normCalledNumber.startsWith("24382") || normCalledNumber.startsWith("24381"))
			{
				outOperator = "Vodacom";
			} else if (normCalledNumber.startsWith("24390"))
			{
				outOperator = "Africell";
			} else
			{
				outOperator = "Unknown";
			}

			// troncation du numéro de tel pour BDD
			if (callingNumber.length() > 19)
			{
				callingNumber = callingNumber.substring(0, 18);
			}
			if (calledNumber.length() > 19)
			{
				calledNumber = calledNumber.substring(0, 18);
			}
			if (normCalledNumber.length() > 19)
			{
				normCalledNumber = normCalledNumber.substring(0, 18);
			}
			translatedNumber = "";
			inTrunkId = getField(IDX_Orig_Trk); // tronc in
			BTS = getField(IDX_Cell_Identity); // BTS
			outTrunkId = getField(IDX_Dest_Trk); // tronc out

			// Cas de Vodacom SSP/RCP tout pourris où le A num et B num peuvent
			// être intervertis
			// ssi fichier SSP et MT qui termine ni chez Vodacom ni chez
			// "unknown" (roaming)
			if (getField(IDX_Filename).contains("SSP") && CallType.matches("MT")
					&& (!outOperator.matches("Vodacom") && !outOperator.matches("Unknown") || inOperator.matches("Vodacom") && outOperator.matches("Unknown")))
			{
				tmpString = callingNumber;
				callingNumber = normCalledNumber;
				normCalledNumber = tmpString;
				calledNumber = getField(IDX_A_Num);
				tmpString = outOperator;
				outOperator = inOperator;
				inOperator = tmpString;
				tmpString = outTrunkId;
				outTrunkId = inTrunkId;
				inTrunkId = tmpString;

			}
			try
			{
				LAC = getField(IDX_LAC); // lac
			} catch (Exception e) // pas de lac dans le cdr si transit
			{
				LAC = "";
			}

			switchID = getField(IDX_Switch_ID);
			callDuration = getField(IDX_Bill_Dur);
			IMSI = getField(IDX_Called_Imsi);
			if (IMSI.isEmpty())
			{
				IMSI = getField(IDX_Calling_Imsi);
			}
			IMEI = getField(IDX_Imei);
			// Set the map group for lookups
			mapGroup = "Vodacom";
			cdrID = getField(IDX_Filename);
			// parse the trafic type
			try
			{
				if (Integer.parseInt(getField(IDX_B_Intl_Ind)) < 2)
				{
					trunkType = "N"; // Indice pour fichier Aggregation
					TrunkTypeID = "2"; // Indice pour BDD
				} else
				{
					trunkType = "I"; // Indice pour fichier Aggregation
					TrunkTypeID = "1"; // Indice pour BDD
				}
			} catch (NumberFormatException nfe)
			{
				RecordError tmpError = new RecordError("ERR_INVALID_OPERATOR_ID", ErrorType.DATA_VALIDATION);
				addError(tmpError);

				// leave - we don't need to process further
				return;
			}

			// parse the timezone
			if (switchID.matches("243818192988") || switchID.matches("243818197988") || switchID.matches("243818198988"))
			{
				timezone = 7600000 * 2;
			} else if (switchID.matches("243818193988"))
			{
				timezone = 0;
			} else
			{
				timezone = 3600000;
			}
			// parse the duration
			try
			{// troncation de durée pour BDD
				duration = Long.parseLong(callDuration);
				if (duration > 65535)
				{
					duration = 65535;
				}
			} catch (NumberFormatException nfe)
			{
				RecordError tmpError = new RecordError("ERR_INVALID_DURATION", ErrorType.DATA_VALIDATION);
				addError(tmpError);
				// leave - we don't need to process further
				return;
			}

			// deal with the initiated date
			try
			{ // Get the call start date from calculation end-duration
				EventStartDate = sdfInput.parse(getField(IDX_Orig_Dt));
				UTCEventDate = EventStartDate.getTime() - timezone; // Set GMT,
																	// no DST
				// EventStartDate = conv.addDateSeconds(EventStartDate,
				// -duration);
				// Arrondi pour horloge interface (intervalle 15 min)
				tmpDate = new Date(300000 * (UTCEventDate / 300000));
				tmpvar = sdfOutput.format(tmpDate).split(" ");
				recordDate = tmpvar[0];
				roundedDate5 = tmpvar[1];

				tmpDate = new Date(UTCEventDate);
				tmpvar = sdfOutput.format(tmpDate).split(" ");
				recordTime = tmpvar[1];
				UTCEventDate = EventStartDate.getTime() / 1000;
				tmpvar = sdfOutput.format(new Date()).split(" ");
				processDate = tmpvar[0];

			} catch (ParseException pex)
			{
				RecordError tmpError = new RecordError("ERR_INVALID_INITIATED_DATE", ErrorType.DATA_VALIDATION);
				addError(tmpError);

				// leave - we don't need to process further
				return;
			}

			// deal with the connected date

			// Set the connection indicator (toujours à 1 car pas d'appels non
			// connectes)
			callConnected = 1;
			PDD = 0;

		}

		// Create a charge packet for the zoning. We don't really need it, but
		// it
		// allows us to use a higher level zoning module
		addChargePacket(newChargePacket());
	}

	public void mapRCPProbeRecord(String dataToMap)
	{
		// Date converter
		SimpleDateFormat sdfInput = new SimpleDateFormat("yyMMddHHmmss");
		SimpleDateFormat sdfOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		// Split record
		fields = dataToMap.replace("}", "").split(";");

		for (int i = 0; i < fields.length; i++)
		{
			// System.out.println("FIELD : " + fields[i]);
			if (fields[i].contains("Allocation time"))
			{
				tmpvar = fields[i].split("=");
				this.recordDate = tmpvar[1];
			} else if (fields[i].contains("Duration"))
			{
				tmpvar = fields[i].split("=");
				this.callDuration = tmpvar[1];
			} else if (fields[i].contains("IMSI"))
			{
				tmpvar = fields[i].split("=");
				this.IMSI = tmpvar[1];
			} else if (fields[i].contains("IMEI"))
			{
				tmpvar = fields[i].split("=");
				this.IMEI = tmpvar[1];
			}
			// else if (fields[i].contains("MSC Identity"))
			// {
			// tmpvar = fields[i].split("=");
			// this.SwitchID = tmpvar[1].substring(2).replaceFirst("^0+(?!$)",
			// "");
			// }
			else if (fields[i].contains("Calling Party"))
			{
				tmpvar = fields[i].split("=");
//				this.callingNumber = tmpvar[1].substring(2).replaceFirst("^0+(?!$)", "");
				this.callingNumber = tmpvar[1].replaceFirst("^0+(?!$)", "");
			} else if (fields[i].contains("Call Partner"))
			{
				tmpvar = fields[i].split("=");
//				this.calledNumber = tmpvar[1].substring(2).replaceFirst("^0+(?!$)", "");
				this.calledNumber = tmpvar[1].replaceFirst("^0+(?!$)", "");
			} else if (fields[i].contains("Call Type"))
			{
				tmpvar = fields[i].split("=");
				this.CallType = tmpvar[1].trim();
			}

			// else if (fields[i].contains("InTrunk"))
			// {
			// tmpvar = fields[i].split("=");
			// this.inTrunkId = tmpvar[1];
			// }
			// else if (fields[i].contains("OutTrunk"))
			// {
			// tmpvar = fields[i].split("=");
			// this.outTrunkId = tmpvar[1];
			// }
			// else if (fields[i].contains("recordNumber"))
			// {
			// tmpvar = fields[i].split("=");
			// this.RecordNumber = Integer.parseInt(tmpvar[1]);
			// }
			// else if (fields[i].contains("LAC"))
			// {
			// tmpvar = fields[i].split("=");
			// this.LAC = tmpvar[1];
			// }
			// else if (fields[i].contains("CellId"))
			// {
			// tmpvar = fields[i].split("=");
			// this.BTS = tmpvar[1];
			// }
			//
		}
		// tmpvar = fields[0].split("= ");
		// this.CallType = tmpvar[1].replace("<", "");

		if ((!CallType.contains("0")) && (!CallType.contains("1")))
		{ // on ne traite que des MO et des MT
			return;
		}
		//
		// // Map basic input
		RECORD_TYPE = PROBE_RECORD;
		//
		// // Check we have a long enough string to work on
		// // Split up the data
		recordType = "Default";
		// //Conservation du B num original, on travaille par la suite avec
		// normCalledNumber
		if ((calledNumber.length() == 9) && ((calledNumber.startsWith("8")) || (calledNumber.startsWith("9")) || (calledNumber.startsWith("1"))))
		{
			normCalledNumber = "243" + calledNumber;
		} else
		{
			normCalledNumber = calledNumber;
		}
		if ((callingNumber.length() == 9) && ((callingNumber.startsWith("8")) || (callingNumber.startsWith("9")) || (callingNumber.startsWith("1"))))
		{
			callingNumber = "243" + callingNumber;
		}

		// detection du tronc in puisque pas de portabilité de num
		if (callingNumber.startsWith("24397") || callingNumber.startsWith("24399"))
		{
			inOperator = "Airtel";
		} else if (callingNumber.startsWith("24384") || callingNumber.startsWith("24385"))
		{
			inOperator = "Orange";
		} else if (callingNumber.startsWith("24315") || callingNumber.startsWith("24312"))
		{
			inOperator = "Standard Telecom";
		} else if (callingNumber.startsWith("24389") || callingNumber.startsWith("24390"))
		{
			inOperator = "Tigo";
		} else if (callingNumber.startsWith("24382") || callingNumber.startsWith("24381"))
		{
			inOperator = "Vodacom";
		} else if (callingNumber.startsWith("24397"))
		{
			inOperator = "Africell";
		} else
		{
			inOperator = "Unknown";
		}

		// detection du tronc out puisque pas de portabilité de num
		if (normCalledNumber.startsWith("24397") || normCalledNumber.startsWith("24399"))
		{
			outOperator = "Airtel";
		} else if (normCalledNumber.startsWith("24384") || normCalledNumber.startsWith("24385"))
		{
			outOperator = "Orange";
		} else if (normCalledNumber.startsWith("24315") || normCalledNumber.startsWith("24312"))
		{
			outOperator = "Standard Telecom";
		} else if (normCalledNumber.startsWith("24389") || normCalledNumber.startsWith("24390"))
		{
			outOperator = "Tigo";
		} else if (normCalledNumber.startsWith("24382") || normCalledNumber.startsWith("24381"))
		{
			outOperator = "Vodacom";
		} else if (normCalledNumber.startsWith("24397"))
		{
			outOperator = "Africell";
		} else
		{
			outOperator = "Unknown";
		}

		// troncation du numéro de tel pour BDD
		if (callingNumber.length() > 19)
		{
			callingNumber = callingNumber.substring(0, 18);
		}
		if (calledNumber.length() > 19)
		{
			calledNumber = calledNumber.substring(0, 18);
		}
		if (normCalledNumber.length() > 19)
		{
			normCalledNumber = normCalledNumber.substring(0, 18);
		}
		//
		// // Set the map group for lookups
		mapGroup = "Vodacom";
		//
		//
		// //------- A fixer -------------
		if (outTrunkId == null)
		{
			outTrunkId = "NULL";
		}
		if (inTrunkId == null)
		{
			inTrunkId = "NULL";
		}
		if (IMEI == null)
		{
			IMEI = "0";
		}
		TrunksSingleton ts = TrunksSingleton.getInstance();
		if (ts.getTrunkTypes().contains(inTrunkId) || ts.getTrunkTypes().contains(outTrunkId))
		{
			trunkType = "I"; // Indice pour fichier Aggregation
			TrunkTypeID = "1"; // Indice pour BDD
		} else
		{
			trunkType = "N"; // Indice pour fichier Aggregation
			TrunkTypeID = "2"; // Indice pour BDD
		}

		// ---------------------------------

		// parse the call type
		switch (CallType) { // "1" = MO - Voice "2" = MT - Voice "3" = Call
							// Forwarding
		case "0":
			CallType = "MO"; // Indice pour fichier aggregation
			CallTypeID = "1"; // Indice pour BDD
			break;
		case "1":
			CallType = "MT"; // Indice pour fichier aggregation
			CallTypeID = "2"; // Indice pour BDD
			break;
		}

		// parse the duration
		try
		{// troncation de durée pour BDD
			duration = Long.parseLong(callDuration);
			if (duration > 65535)
			{
				duration = 65535;
			}
		} catch (NumberFormatException nfe)
		{
			RecordError tmpError = new RecordError("ERR_INVALID_DURATION", ErrorType.DATA_VALIDATION);
			addError(tmpError);
			// leave - we don't need to process further
			return;
		}
		// // deal with the initiated date
		timezone = 0;
		try
		{ // Get the call start date from calculation end-duration
			tmpvar = recordDate.split("2b");
			recordDate = tmpvar[0];
			EventStartDate = sdfInput.parse(recordDate);
			UTCEventDate = EventStartDate.getTime() - 3600000; // Set GMT, no
																// DST
			// EventStartDate = conv.addDateSeconds(EventStartDate, -duration);
			// Arrondi pour horloge interface (intervalle 15 min)
			tmpDate = new Date(300000 * (UTCEventDate / 300000));
			tmpvar = sdfOutput.format(tmpDate).split(" ");
			recordDate = tmpvar[0];
			roundedDate5 = tmpvar[1];

			tmpDate = new Date(UTCEventDate);
			tmpvar = sdfOutput.format(tmpDate).split(" ");
			recordTime = tmpvar[1];
			UTCEventDate = EventStartDate.getTime() / 1000;
			tmpDate = new Date();
			tmpvar = sdfOutput.format(tmpDate).split(" ");
			processDate = tmpvar[0];

		} catch (ParseException pex)
		{
			RecordError tmpError = new RecordError("ERR_INVALID_INITIATED_DATE", ErrorType.DATA_VALIDATION);
			addError(tmpError);

			// leave - we don't need to process further
			return;
		}

		// deal with the connected date

		// Set the connection indicator (toujours à 1 car pas d'appels non
		// connectes)
		callConnected = 1;
		PDD = 0;

		// }

		// Create a charge packet for the zoning. We don't really need it, but
		// it
		// allows us to use a higher level zoning module
		addChargePacket(newChargePacket());
	}

	public void mapSSPProbeRecord(String dataToMap)
	{
		// Date converter
		SimpleDateFormat sdfInput = new SimpleDateFormat("yyMMddHHmmss");
		SimpleDateFormat sdfOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		// Split record
		fields = dataToMap.replace("}", "").split(";");

		for (int i = 0; i < fields.length; i++)
		{
			// System.out.println("FIELD : " + fields[i]);
			if (fields[i].contains("Allocation time"))
			{
				tmpvar = fields[i].split("=");
				this.recordDate = tmpvar[1];
			} else if (fields[i].contains("Duration"))
			{
				tmpvar = fields[i].split("=");
				this.callDuration = tmpvar[1];
			} else if (fields[i].contains("IMSI"))
			{
				tmpvar = fields[i].split("=");
				this.IMSI = tmpvar[1];
			} else if (fields[i].contains("IMEI"))
			{
				tmpvar = fields[i].split("=");
				this.IMEI = tmpvar[1];
			}
			// else if (fields[i].contains("MSC Identity"))
			// {
			// tmpvar = fields[i].split("=");
			// this.SwitchID = tmpvar[1].substring(2).replaceFirst("^0+(?!$)",
			// "");
			// }
			else if (fields[i].contains("Calling Party"))
			{
				tmpvar = fields[i].split("=");
//				this.callingNumber = tmpvar[1].substring(2).replaceFirst("^0+(?!$)", "");
				this.callingNumber = tmpvar[1].replaceFirst("^0+(?!$)", "");
			} else if (fields[i].contains("Called Party"))
			{
				tmpvar = fields[i].split("=");
//				this.calledNumber = tmpvar[1].substring(2).replaceFirst("^0+(?!$)", "");
				this.calledNumber = tmpvar[1].replaceFirst("^0+(?!$)", "");
			} else if (fields[i].contains("Call Type"))
			{
				tmpvar = fields[i].split("=");
				this.CallType = tmpvar[1].trim();
			}

			else if (fields[i].contains("Incoming Trunk"))
			{
				tmpvar = fields[i].split("=");
				this.inTrunkId = tmpvar[1];
			} else if (fields[i].contains("Outgoing Trunk"))
			{
				tmpvar = fields[i].split("=");
				this.outTrunkId = tmpvar[1];
			}
			// else if (fields[i].contains("recordNumber"))
			// {
			// tmpvar = fields[i].split("=");
			// this.RecordNumber = Integer.parseInt(tmpvar[1]);
			// }
			// else if (fields[i].contains("LAC"))
			// {
			// tmpvar = fields[i].split("=");
			// this.LAC = tmpvar[1];
			// }
			// else if (fields[i].contains("CellId"))
			// {
			// tmpvar = fields[i].split("=");
			// this.BTS = tmpvar[1];
			// }
			//
		}
		// tmpvar = fields[0].split("= ");
		// this.CallType = tmpvar[1].replace("<", "");

		if ((!CallType.equals("10")))
		{ // on ne traite que des MO et des MT, pas les Transit
			return;
		}
		//
		// // Map basic input
		RECORD_TYPE = PROBE_RECORD;
		//
		// // Check we have a long enough string to work on
		// // Split up the data
		recordType = "Default";
		// //Conservation du B num original, on travaille par la suite avec
		// normCalledNumber
		if ((calledNumber.length() == 9) && ((calledNumber.startsWith("8")) || (calledNumber.startsWith("9")) || (calledNumber.startsWith("1"))))
		{
			normCalledNumber = "243" + calledNumber;
		} else
		{
			normCalledNumber = calledNumber;
		}
		if ((callingNumber.length() == 9) && ((callingNumber.startsWith("8")) || (callingNumber.startsWith("9")) || (callingNumber.startsWith("1"))))
		{
			callingNumber = "243" + callingNumber;
		}

		// detection du tronc in puisque pas de portabilité de num
		if (callingNumber.startsWith("24397") || callingNumber.startsWith("24399"))
		{
			inOperator = "Airtel";
		} else if (callingNumber.startsWith("24384") || callingNumber.startsWith("24385"))
		{
			inOperator = "Orange";
		} else if (callingNumber.startsWith("24315") || callingNumber.startsWith("24312"))
		{
			inOperator = "Standard Telecom";
		} else if (callingNumber.startsWith("24389") || callingNumber.startsWith("24390"))
		{
			inOperator = "Tigo";
		} else if (callingNumber.startsWith("24382") || callingNumber.startsWith("24381"))
		{
			inOperator = "Vodacom";
		} else if (callingNumber.startsWith("24397"))
		{
			inOperator = "Africell";
		} else
		{
			inOperator = "Unknown";
		}

		// detection du tronc out puisque pas de portabilité de num
		if (normCalledNumber.startsWith("24397") || normCalledNumber.startsWith("24399"))
		{
			outOperator = "Airtel";
		} else if (normCalledNumber.startsWith("24384") || normCalledNumber.startsWith("24385"))
		{
			outOperator = "Orange";
		} else if (normCalledNumber.startsWith("24315") || normCalledNumber.startsWith("24312"))
		{
			outOperator = "Standard Telecom";
		} else if (normCalledNumber.startsWith("24389") || normCalledNumber.startsWith("24390"))
		{
			outOperator = "Tigo";
		} else if (normCalledNumber.startsWith("24382") || normCalledNumber.startsWith("24381"))
		{
			outOperator = "Vodacom";
		} else if (normCalledNumber.startsWith("24397"))
		{
			outOperator = "Africell";
		} else
		{
			outOperator = "Unknown";
		}

		// troncation du numéro de tel pour BDD
		if (callingNumber.length() > 19)
		{
			callingNumber = callingNumber.substring(0, 18);
		}
		if (calledNumber.length() > 19)
		{
			calledNumber = calledNumber.substring(0, 18);
		}
		if (normCalledNumber.length() > 19)
		{
			normCalledNumber = normCalledNumber.substring(0, 18);
		}
		//
		// // Set the map group for lookups
		mapGroup = "Vodacom";
		//
		//
		// //------- A fixer -------------
		if (outTrunkId == null)
		{
			outTrunkId = "NULL";
		}
		if (inTrunkId == null)
		{
			inTrunkId = "NULL";
		}
		if (IMEI == null)
		{
			IMEI = "0";
		}
		try
		{
			TrunksSingleton ts = TrunksSingleton.getInstance();
			if (ts.getTrunkTypes().contains(inTrunkId) || ts.getTrunkTypes().contains(outTrunkId))
			{
				trunkType = "I"; // Indice pour fichier Aggregation
				TrunkTypeID = "1"; // Indice pour BDD
			} else
			{
				trunkType = "N"; // Indice pour fichier Aggregation
				TrunkTypeID = "2"; // Indice pour BDD
			}

		} catch (NumberFormatException nfe)
		{
			RecordError tmpError = new RecordError("ERR_INVALID_OPERATOR_ID", ErrorType.DATA_VALIDATION);
			addError(tmpError);

			// leave - we don't need to process further
			return;
		}

		// ---------------------------------

		// parse the call type
		switch (CallType) { // "10" = MO 
		case "10":
			CallType = "MO"; // Indice pour fichier aggregation
			CallTypeID = "1"; // Indice pour BDD
			break;
		}

		// parse the duration
		try
		{// troncation de durée pour BDD
			duration = Long.parseLong(callDuration);
			if (duration > 65535)
			{
				duration = 65535;
			}
		} catch (NumberFormatException nfe)
		{
			RecordError tmpError = new RecordError("ERR_INVALID_DURATION", ErrorType.DATA_VALIDATION);
			addError(tmpError);
			// leave - we don't need to process further
			return;
		}
		// // deal with the initiated date
		timezone = 0;
		try
		{ // Get the call start date from calculation end-duration
			tmpvar = recordDate.split("2b");
			recordDate = tmpvar[0];
			EventStartDate = sdfInput.parse(recordDate);
			UTCEventDate = EventStartDate.getTime() - 3600000; // Set GMT, no
																// DST
			// EventStartDate = conv.addDateSeconds(EventStartDate, -duration);
			// Arrondi pour horloge interface (intervalle 15 min)
			tmpDate = new Date(300000 * (UTCEventDate / 300000));
			tmpvar = sdfOutput.format(tmpDate).split(" ");
			recordDate = tmpvar[0];
			roundedDate5 = tmpvar[1];

			tmpDate = new Date(UTCEventDate);
			tmpvar = sdfOutput.format(tmpDate).split(" ");
			recordTime = tmpvar[1];
			UTCEventDate = EventStartDate.getTime() / 1000;
			tmpDate = new Date();
			tmpvar = sdfOutput.format(tmpDate).split(" ");
			processDate = tmpvar[0];

		} catch (ParseException pex)
		{
			RecordError tmpError = new RecordError("ERR_INVALID_INITIATED_DATE", ErrorType.DATA_VALIDATION);
			addError(tmpError);

			// leave - we don't need to process further
			return;
		}

		// deal with the connected date

		// Set the connection indicator (toujours à 1 car pas d'appels non
		// connectes)
		callConnected = 1;
		PDD = 0;

		// }

		// Create a charge packet for the zoning. We don't really need it, but
		// it
		// allows us to use a higher level zoning module
		addChargePacket(newChargePacket());
	}

	public void map3GPPNGNProbeRecord(String dataToMap)
	{
		// Date converter
		SimpleDateFormat sdfInput = new SimpleDateFormat("yyMMddHHmmss");
		SimpleDateFormat sdfOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		// Split record
		fields = dataToMap.replace("}", "").split(";");

		for (int i = 0; i < fields.length; i++)
		{
			// System.out.println("FIELD : " + fields[i]);
			if (fields[i].contains("answerTime"))
			{
				tmpvar = fields[i].split("=");
				this.recordDate = tmpvar[1];
			} else if (fields[i].contains("Duration"))
			{
				tmpvar = fields[i].split("=");
				this.callDuration = tmpvar[1];
			} else if (fields[i].contains("IMSI"))
			{
				tmpvar = fields[i].split("=");
				this.IMSI = tmpvar[1];
			} else if (fields[i].contains("IMEI"))
			{
				tmpvar = fields[i].split("=");
				this.IMEI = tmpvar[1];
			}
			// else if (fields[i].contains("MSC Identity"))
			// {
			// tmpvar = fields[i].split("=");
			// this.SwitchID = tmpvar[1].substring(2).replaceFirst("^0+(?!$)",
			// "");
			// }
			else if (fields[i].contains("callingNumber"))
			{
				tmpvar = fields[i].split("=");
				this.callingNumber = tmpvar[1].substring(2).replaceFirst("^0+(?!$)", "");
			} else if (fields[i].contains("calledNumber"))
			{
				tmpvar = fields[i].split("=");
				this.calledNumber = tmpvar[1].substring(2).replaceFirst("^0+(?!$)", "");
			} else if (fields[i].contains("CallType"))
			{
				tmpvar = fields[i].split("=");
				this.CallType = tmpvar[1].trim();
			}

			else if (fields[i].contains("inTrunk"))
			{
				tmpvar = fields[i].split("=");
				this.inTrunkId = tmpvar[1];
			} else if (fields[i].contains("outTrunk"))
			{
				tmpvar = fields[i].split("=");
				this.outTrunkId = tmpvar[1];
			}
			// else if (fields[i].contains("recordNumber"))
			// {
			// tmpvar = fields[i].split("=");
			// this.RecordNumber = Integer.parseInt(tmpvar[1]);
			// }
			else if (fields[i].contains("LAC"))
			{
				tmpvar = fields[i].split("=");
				this.LAC = tmpvar[1];
			} else if (fields[i].contains("cellId"))
			{
				tmpvar = fields[i].split("=");
				this.BTS = tmpvar[1];
			}
			//
		}
		tmpvar = fields[0].split("= ");
		this.CallType = tmpvar[1].replace("<", "");

		if ((!CallType.contains("MOC")) && (!CallType.contains("MTC")))
		{ // on ne traite que des MO et des MT
			return;
		}
		//
		// // Map basic input
		RECORD_TYPE = PROBE_RECORD;
		//
		// // Check we have a long enough string to work on
		// // Split up the data
		recordType = "Default";
		// //Conservation du B num original, on travaille par la suite avec
		// normCalledNumber
		if ((calledNumber.length() == 9) && ((calledNumber.startsWith("8")) || (calledNumber.startsWith("9")) || (calledNumber.startsWith("1"))))
		{
			normCalledNumber = "243" + calledNumber;
		} else
		{
			normCalledNumber = calledNumber;
		}
		if ((callingNumber.length() == 9) && ((callingNumber.startsWith("8")) || (callingNumber.startsWith("9")) || (callingNumber.startsWith("1"))))
		{
			callingNumber = "243" + callingNumber;
		}

		// detection du tronc in puisque pas de portabilité de num
		if (callingNumber.startsWith("24397") || callingNumber.startsWith("24399"))
		{
			inOperator = "Airtel";
		} else if (callingNumber.startsWith("24384") || callingNumber.startsWith("24385"))
		{
			inOperator = "Orange";
		} else if (callingNumber.startsWith("24315") || callingNumber.startsWith("24312"))
		{
			inOperator = "Standard Telecom";
		} else if (callingNumber.startsWith("24389") || callingNumber.startsWith("24390"))
		{
			inOperator = "Tigo";
		} else if (callingNumber.startsWith("24382") || callingNumber.startsWith("24381"))
		{
			inOperator = "Vodacom";
		} else if (callingNumber.startsWith("24397"))
		{
			inOperator = "Africell";
		} else
		{
			inOperator = "Unknown";
		}

		// detection du tronc out puisque pas de portabilité de num
		if (normCalledNumber.startsWith("24397") || normCalledNumber.startsWith("24399"))
		{
			outOperator = "Airtel";
		} else if (normCalledNumber.startsWith("24384") || normCalledNumber.startsWith("24385"))
		{
			outOperator = "Orange";
		} else if (normCalledNumber.startsWith("24315") || normCalledNumber.startsWith("24312"))
		{
			outOperator = "Standard Telecom";
		} else if (normCalledNumber.startsWith("24389") || normCalledNumber.startsWith("24390"))
		{
			outOperator = "Tigo";
		} else if (normCalledNumber.startsWith("24382") || normCalledNumber.startsWith("24381"))
		{
			outOperator = "Vodacom";
		} else if (normCalledNumber.startsWith("24397"))
		{
			outOperator = "Africell";
		} else
		{
			outOperator = "Unknown";
		}

		// troncation du numéro de tel pour BDD
		if (callingNumber.length() > 19)
		{
			callingNumber = callingNumber.substring(0, 18);
		}
		if (calledNumber.length() > 19)
		{
			calledNumber = calledNumber.substring(0, 18);
		}
		if (normCalledNumber.length() > 19)
		{
			normCalledNumber = normCalledNumber.substring(0, 18);
		}
		//
		// // Set the map group for lookups
		mapGroup = "Vodacom";
		//
		//
		// //------- A fixer -------------
		if (outTrunkId == null)
		{
			outTrunkId = "NULL";
		}
		if (inTrunkId == null)
		{
			inTrunkId = "NULL";
		}
		if (IMEI == null)
		{
			IMEI = "0";
		}
		if (inOperator.equals("Unknown") || outOperator.equals("Unknown"))
		{
			trunkType = "I"; // Indice pour fichier Aggregation
			TrunkTypeID = "1"; // Indice pour BDD
		} else
		{
			trunkType = "N"; // Indice pour fichier Aggregation
			TrunkTypeID = "2"; // Indice pour BDD
		}

		// ---------------------------------

		// parse the call type
		switch (CallType) { // "1" = MO - Voice "2" = MT - Voice "3" = Call
							// Forwarding
		case "MOC":
			CallType = "MO"; // Indice pour fichier aggregation
			CallTypeID = "1"; // Indice pour BDD
			break;
		case "MTC":
			CallType = "MT"; // Indice pour fichier aggregation
			CallTypeID = "2"; // Indice pour BDD
			break;
		}

		// parse the duration
		try
		{// troncation de durée pour BDD
			duration = Long.parseLong(callDuration);
			if (duration > 65535)
			{
				duration = 65535;
			}
		} catch (NumberFormatException nfe)
		{
			RecordError tmpError = new RecordError("ERR_INVALID_DURATION", ErrorType.DATA_VALIDATION);
			addError(tmpError);
			// leave - we don't need to process further
			return;
		}
		// // deal with the initiated date
		timezone = 0;
		try
		{ // Get the call start date from calculation end-duration
			tmpvar = recordDate.split("2b");
			recordDate = tmpvar[0];
			EventStartDate = sdfInput.parse(recordDate);
			UTCEventDate = EventStartDate.getTime() - 3600000; // Set GMT, no
																// DST
			// EventStartDate = conv.addDateSeconds(EventStartDate, -duration);
			// Arrondi pour horloge interface (intervalle 15 min)
			tmpDate = new Date(300000 * (UTCEventDate / 300000));
			tmpvar = sdfOutput.format(tmpDate).split(" ");
			recordDate = tmpvar[0];
			roundedDate5 = tmpvar[1];

			tmpDate = new Date(UTCEventDate);
			tmpvar = sdfOutput.format(tmpDate).split(" ");
			recordTime = tmpvar[1];
			UTCEventDate = EventStartDate.getTime() / 1000;
			tmpDate = new Date();
			tmpvar = sdfOutput.format(tmpDate).split(" ");
			processDate = tmpvar[0];

		} catch (ParseException pex)
		{
			RecordError tmpError = new RecordError("ERR_INVALID_INITIATED_DATE", ErrorType.DATA_VALIDATION);
			addError(tmpError);

			// leave - we don't need to process further
			return;
		}

		// deal with the connected date

		// Set the connection indicator (toujours à 1 car pas d'appels non
		// connectes)
		callConnected = 1;
		PDD = 0;

		// }

		// Create a charge packet for the zoning. We don't really need it, but
		// it
		// allows us to use a higher level zoning module
		addChargePacket(newChargePacket());
	}

	/**
	 * Utility function to unmap a file header or trailer record
	 * 
	 * @return The built information the header trailer
	 */
	public String unMapProbeHeaderTrailer(long RecordCount, double Cost)
	{
		if (RECORD_TYPE == ProbeRecord.HEADER_RECORD)
		{
			// Headers and trailers
			return OriginalData;
		} else if (RECORD_TYPE == ProbeRecord.TRAILER_RECORD)
		{
			// Headers and trailers
			return OriginalData;
		} else
		{
			return null;
		}
	}

	/**
	 * Utility function to unmap a processed Probe Record
	 * 
	 * @return The built information for the Probe Output File
	 */
	public String unMapProbeRecord()
	{
		if (RECORD_TYPE == ProbeRecord.PROBE_RECORD)
		{
			// Rebuild record
			StringBuilder outputRec = new StringBuilder();

			// Operator
			outputRec = outputRec.append(mapGroup).append(ProbeRecord.FIELD_SPLITTER);

			// Call Date
			outputRec = outputRec.append(recordDate).append(ProbeRecord.FIELD_SPLITTER);

			// Call Time
			outputRec = outputRec.append(recordTime).append(ProbeRecord.FIELD_SPLITTER);

			// PDD
			outputRec = outputRec.append(Long.toString(PDD)).append(ProbeRecord.FIELD_SPLITTER);

			// Duration
			outputRec = outputRec.append(Long.toString(duration)).append(ProbeRecord.FIELD_SPLITTER);

			// Calling Number
			outputRec = outputRec.append(callingNumber).append(ProbeRecord.FIELD_SPLITTER);

			// Called Number
			outputRec = outputRec.append(matchCalledNumber).append(ProbeRecord.FIELD_SPLITTER);

			// BTS
			outputRec = outputRec.append(BTS).append(ProbeRecord.FIELD_SPLITTER);

			// LAC
			outputRec = outputRec.append(LAC).append(ProbeRecord.FIELD_SPLITTER);

			// Imei
			outputRec = outputRec.append(IMEI).append(ProbeRecord.FIELD_SPLITTER);

			// IMSI
			outputRec = outputRec.append(IMSI).append(ProbeRecord.FIELD_SPLITTER);

			// Operateur In
			outputRec = outputRec.append(inOperator).append(ProbeRecord.FIELD_SPLITTER);

			// Tronc In
			outputRec = outputRec.append(inTrunkId).append(ProbeRecord.FIELD_SPLITTER);

			// Operateur Out
			outputRec = outputRec.append(outOperator).append(ProbeRecord.FIELD_SPLITTER);

			// Tronc Out
			outputRec = outputRec.append(outTrunkId).append(ProbeRecord.FIELD_SPLITTER);

			// Call type - MO/MT
			outputRec = outputRec.append(CallType).append(ProbeRecord.FIELD_SPLITTER);

			// Destination - no trailing separator
			outputRec = outputRec.append(destination).append(ProbeRecord.FIELD_SPLITTER);

			// Trunk Type - International or National
			outputRec = outputRec.append(trunkType);

			return outputRec.toString();
		}

		if (RECORD_TYPE == ProbeRecord.LIVE_TRACK_RECORD)
		{
			// Rebuild record
			StringBuilder outputRec = new StringBuilder();

			// Operator
			outputRec = outputRec.append(mapGroup).append(ProbeRecord.FIELD_SPLITTER);

			// Call Date
			outputRec = outputRec.append(recordDate).append(ProbeRecord.FIELD_SPLITTER);

			// Call Time
			outputRec = outputRec.append(recordTime).append(ProbeRecord.FIELD_SPLITTER);

			// PDD
			outputRec = outputRec.append(Long.toString(PDD)).append(ProbeRecord.FIELD_SPLITTER);

			// Duration
			outputRec = outputRec.append(Long.toString(duration)).append(ProbeRecord.FIELD_SPLITTER);

			// Calling Number
			outputRec = outputRec.append(callingNumber).append(ProbeRecord.FIELD_SPLITTER);

			// Called Number
			outputRec = outputRec.append(matchCalledNumber).append(ProbeRecord.FIELD_SPLITTER);

			// Normalised Called Number
			// outputRec =
			// outputRec.append(calledNumber).append(ProbeRecord.FIELD_SPLITTER);

			// In Trunk Name
			outputRec = outputRec.append(inTrunkName).append(ProbeRecord.FIELD_SPLITTER);

			// In Trunk ID
			outputRec = outputRec.append(inTrunkId).append(ProbeRecord.FIELD_SPLITTER);

			// Out Trunk Name
			outputRec = outputRec.append(outTrunkName).append(ProbeRecord.FIELD_SPLITTER);

			// Out Trunk ID
			outputRec = outputRec.append(outTrunkId).append(ProbeRecord.FIELD_SPLITTER);

			// Destination - no trailing separator
			outputRec = outputRec.append(destination).append(ProbeRecord.FIELD_SPLITTER);

			// Trunk Type - International or National
			outputRec = outputRec.append(trunkType);

			return outputRec.toString();
		}

		return "ToDo";
	}

	/**
	 * Utility function to unmap a processed Probe Record
	 * 
	 * @return The built information for the Probe Output File
	 */
	public String unMapEmtechProbeRecordReject()
	{
		if (RECORD_TYPE == ProbeRecord.PROBE_RECORD)
		{
			// Rebuild record
			StringBuilder outputRec = new StringBuilder();

			// Add probe header
			outputRec = outputRec.append(RECYCLE_TAG).append(ProbeRecord.FIELD_SPLITTER);

			// Add error
			outputRec = outputRec.append(getErrors().get(0).getMessage()).append(ProbeRecord.FIELD_SPLITTER);

			// Add recycle count
			outputRec = outputRec.append(recycleCount).append(ProbeRecord.FIELD_SPLITTER);

			// add the rest of the record
			// outputRec = outputRec.append(OriginalData);
			// CDR ID
			outputRec = outputRec.append(cdrID).append(ProbeRecord.FIELD_SPLITTER);
			// Call Date
			outputRec = outputRec.append(recordDate).append(ProbeRecord.FIELD_SPLITTER);
			// Call Time
			outputRec = outputRec.append(recordTime).append(ProbeRecord.FIELD_SPLITTER);
			return outputRec.toString();
		}

		return null;
	}

	/**
	 * Return the dump-ready data
	 * 
	 * @return The built information for dumping
	 */
	@Override
	public ArrayList<String> getDumpInfo()
	{
		ArrayList<String> tmpDumpList;
		tmpDumpList = new ArrayList<String>();

		if ((RECORD_TYPE == ProbeRecord.PROBE_RECORD) || (RECORD_TYPE == ProbeRecord.PROFILE_RECORD))
		{
			tmpDumpList.add("============ BEGIN RECORD ============");
			tmpDumpList.add("  Outputs                    = <" + getOutputs() + ">");
			tmpDumpList.add("  ---------------");
			tmpDumpList.add("  recordType                 = <" + recordType + ">");
			tmpDumpList.add("  MSISDN                     = <" + MSISDN + ">");
			tmpDumpList.add("  callingNumber              = <" + callingNumber + ">");
			tmpDumpList.add("  calledNumber               = <" + calledNumber + ">");
			tmpDumpList.add("  Norm Calling Number        = <" + normCallingNumber + ">");
			tmpDumpList.add("  Norm Called Number         = <" + normCalledNumber + ">");
			tmpDumpList.add("  Num_Origin                 = _" + recordDate + "_" + mapGroup + "_" + callingNumber + "_" + trunkType + "_");
			tmpDumpList.add("  Obfsucated Calling Number  = <" + matchCallingNumber + ">");
			tmpDumpList.add("  Obfuscated Called Number   = <" + matchCalledNumber + ">");
			tmpDumpList.add("  translatedNumber           = <" + translatedNumber + ">");
			tmpDumpList.add("  inOperator                 = <" + inOperator + ">");
			tmpDumpList.add("  outOperator                = <" + outOperator + ">");
			tmpDumpList.add("  eventTime                  = <" + EventStartDate + ">");
			tmpDumpList.add("  Connection Time            = <" + connectTime + ">");
			tmpDumpList.add("  callDuration               = <" + callDuration + ">");
			tmpDumpList.add("  Pending Duration Delay     = <" + PDD + ">");
			tmpDumpList.add("  ---------------");
			tmpDumpList.add("  Probe Record               = <" + probeRecord + ">");
			tmpDumpList.add("  In Trunk ID                = <" + inTrunkId + ">");
			tmpDumpList.add("  In Trunk Name              = <" + inTrunkName + ">");
			tmpDumpList.add("  Out Trunk ID               = <" + outTrunkId + ">");
			tmpDumpList.add("  Out Trunk Name             = <" + outTrunkName + ">");
			tmpDumpList.add("  Trunk Type                 = <" + trunkType + ">");
			tmpDumpList.add("  Destination Number         = <" + normCalledNumber + ">");
			tmpDumpList.add("  Destination                = <" + destination + ">");
			tmpDumpList.add("  Call Connected             = <" + callConnected + ">");
		}

		// Add error info
		tmpDumpList.addAll(getErrorDump(22));

		return tmpDumpList;
	}
}
